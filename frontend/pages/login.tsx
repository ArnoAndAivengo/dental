import {Field, Formik} from "formik";
import Router from "next/router";
import React from "react";
import {InputField} from "../components/Form/fields/InputField";
import Layout from "../components/Layout";
import Avatar from "../components/Form/Avatar";
import Typography from "../components/Typography";
import FooterForm from "../components/FooterForm";
import {LoginComponent, MeQuery} from "../generated/apolloComponents";
import {meQuery} from "../graphql/dentist/queries/me";
import ButtonForm from "../components/Buttons/ButtonForm";
import {Image, RightSide} from "../styles/Main.module";
import {Grid} from "@material-ui/core";
import Link from "next/link";

const Login = () => {
  return (
    <Layout title="Login page">
      <Grid container>
        <Grid item xs={6} sm={6} lg={6}>
          <Image/>
        </Grid>
        <Grid item xs={6} sm={6} lg={6}>
          <RightSide>
            <Avatar/>
            <Typography title='Login'/>
            <LoginComponent>
              {login => (
                <Formik
                  validateOnBlur={false}
                  validateOnChange={false}
                  onSubmit={async (data, {setErrors}) => {
                    const response = await login({
                      variables: data,
                      update: (cache, {data}) => {
                        if (!data || !data.login) {
                          return;
                        }

                        cache.writeQuery<MeQuery>({
                          query: meQuery,
                          data: {
                            __typename: "Query",
                            // @ts-ignore
                            me: data.login
                          }
                        });
                      }
                    });
                    if (response && response.data && !response.data.login) {
                      setErrors({
                        email: "invalid login"
                      });
                      return;
                    }

                    await Router.push("/search");
                  }}
                  initialValues={{
                    email: "",
                    password: ""
                  }}
                >
                  {({handleSubmit}) => (
                    <form onSubmit={handleSubmit}>
                      <Field
                        name="email"
                        placeholder="email"
                        component={InputField}
                      />
                      <Field
                        name="password"
                        placeholder="password"
                        type="password"
                        component={InputField}
                      />
                      <ButtonForm title='Sign In'>submit</ButtonForm>
                    </form>
                  )}
                </Formik>
              )}
            </LoginComponent>
            <Link href={"../register"}>Create your Dental account</Link>
            <Link href={"../search"}>Go to search Dentist</Link>
            <FooterForm/>
          </RightSide>
        </Grid>
      </Grid>
    </Layout>
  );
};

export default Login;

import * as React from "react";
import Layout from "../components/Layout";
import styled from "styled-components";

const CheckWrapper = styled("div")`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100vh;
`;

const checkEmail = () => {
  return (
    <Layout title="Check Email">
      <CheckWrapper>
        Check your email to confirm your account
      </CheckWrapper>
    </Layout>
  );
};

export default checkEmail

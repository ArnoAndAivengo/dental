import * as React from "react";
import {
  ConfirmDentistMutation,
  ConfirmDentistMutationVariables
} from "../generated/apolloComponents";
import { confirmDentistMutation } from "../graphql/dentist/mutations/confirmDentist";
import { MyContext } from "../interfaces/MyContext";
import redirect from "../lib/redirect";

export default class Confirm extends React.PureComponent {
  static async getInitialProps({
    query: { slug },
    apolloClient,
    ...ctx
  }: MyContext) {


    if (!slug) {
      return {};
    }

    await apolloClient.mutate<ConfirmDentistMutation, ConfirmDentistMutationVariables>({
      mutation: confirmDentistMutation,
      variables: {
        token: slug as string
      }
    });
    redirect(ctx, "/login");

    return {};
  }

  render() {
    return "something went wrong";
  }
}

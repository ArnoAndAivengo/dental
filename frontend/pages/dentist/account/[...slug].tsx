import * as React from "react";
import {useGetIntId} from "../../../utils/useGetIntId";
import {MeComponent, useDentistQuery} from "../../../generated/apolloComponents";
import Layout from "../../../components/Layout";
import Header from "../../../components/Header";
import Drawer from "../../../components/Drawer";
import GalleryComponent from "../../../components/Gallery";
import Breadcrumb from "../../../components/Breadcrumb";
import AvatarProfile from "../../../components/Dentist/Avatar";
import DentistProfileInfo from "../../../components/Dentist/Info";
import Services from "../../../components/Dentist/Services";
import Practises from "../../../components/Dentist/Practises";
import {useState} from "react";
import {Grid} from "@material-ui/core";

import {FlexWrapper, Box, MainContainer} from "../../../styles/Main.module";

const Account = ({}) => {
  const intId = useGetIntId();
  const {data, loading, error} = useDentistQuery({
    variables: {
      id: Number(intId)
    },
  });

  if (error) {
    return (
      <>Error</>
    )
  }

  const [images, setImages] = useState([]);
  const URL: string = "http://localhost:4000/files/" + intId

  const getImages = async () => {
    const requestOptions: {} = {
      method: 'GET',
      redirect: 'follow'
    };

    await fetch(URL, requestOptions)
      .then(response => response.json())
      .then(result => {
        setImages(result)
      })
      .catch((_error: any) => {
      })
  }

  return (
    !loading ? <Layout title="Profile">
      <Box>
        <Header/>
        <FlexWrapper>
          <MeComponent>
            {({data, loading}) => {
              if (!data || loading || !data.me) {
                return null;
              }
              return (
                <Drawer/>
              );
            }}
          </MeComponent>
          <MainContainer>
            <Breadcrumb point="Account"/>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={4} lg={2}>
                <AvatarProfile data={data?.dentist}/>
              </Grid>
              <Grid item xs={12} sm={8} lg={4}>
                <DentistProfileInfo data={data?.dentist}/>
              </Grid>
              <Grid item xs={12} sm={6} lg={3}>
                <Services data={data?.dentist}/>
              </Grid>
              <Grid item xs={12} sm={6} lg={3}>
                <Practises data={data?.dentist}/>
              </Grid>
            </Grid>
            <GalleryComponent images={images} getImages={getImages}/>
          </MainContainer>
        </FlexWrapper>
      </Box>
    </Layout> : <>...Loading</>
  )

}

export default Account;

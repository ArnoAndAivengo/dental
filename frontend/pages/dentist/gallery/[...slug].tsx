import React, {useState} from "react";
import Drawer from "../../../components/Drawer";
import Header from "../../../components/Header";
import Layout from "../../../components/Layout";
import {InputSearch} from "../../../styles/Search.module";
import {Grid, IconButton} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import DownloadDropzone from "../../../components/Gallery/DownloadDropzone";
import DeleteFile from "../../../components/Gallery/DeleteFile";
import Breadcrumb from "../../../components/Breadcrumb";
import GalleryComponent from "../../../components/Gallery";
import {MeComponent} from "../../../generated/apolloComponents";
import {useGetIntId} from "../../../utils/useGetIntId";

import {FlexWrapper, Box, MainContainer, FormBlockWrapper, Search} from "../../../styles/Main.module";

const Gallery = () => {
  const intId = useGetIntId();
  const [deleteImage, setDeleteImage] = useState([]);
  const [images, setImages] = useState([]);

  const URL: string = "http://localhost:4000/files/" + intId

  const getImages = async () => {
    const requestOptions: {} = {
      method: 'GET',
      redirect: 'follow'
    };

    await fetch(URL, requestOptions)
      .then(response => response.json())
      .then(result => {
        setImages(result)
      })
      .catch((_error: any) => {
      })
  }

  return (
    <Layout title="Profile Gallery">
      <Box>
        <Header/>
        <FlexWrapper>
          <Drawer/>
          <MainContainer>
            <Breadcrumb point="Gallery"/>
            <FormBlockWrapper>
              <Grid container alignItems="center" justify="space-between" spacing={1}>
                <Grid item xs={12} sm={6} lg={6}>
                  <Grid container alignItems="center" justify="space-between">
                    <Grid item xs={12} sm={10} lg={6}>
                      <Search>
                        <IconButton type="submit" aria-label="search"
                                    style={{width: '45px', height: '45px', color: '#0d9da6'}}>
                          <SearchIcon/>
                        </IconButton>
                        <InputSearch
                          style={{width: '70%', background: 'transparent'}}
                          placeholder="Search Images by title"
                        />
                      </Search>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  <Grid container alignItems="center" justify="flex-end" spacing={1}>
                    <Grid item xs={12} sm={5} lg={4}>
                      <DeleteFile
                        // @ts-ignore
                        me={intId} deleteImage={deleteImage} getImages={getImages}/>
                    </Grid>
                    <Grid item xs={12} sm={5} lg={4}>
                      <DownloadDropzone
                        // @ts-ignore
                        images={images} getImages={getImages}/>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </FormBlockWrapper>
            <MeComponent>
              {({data, loading}) => {
                if (!data || loading || !data.me) {
                  return null;
                }
                return (
                  <GalleryComponent
                    setDeleteImage={setDeleteImage}
                    // @ts-ignore
                    images={images} getImages={getImages}/>
                )
              }}
            </MeComponent>
          </MainContainer>
        </FlexWrapper>
      </Box>
    </Layout>
  );
};

export default Gallery

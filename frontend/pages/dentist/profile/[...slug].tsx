import React from "react";
import Header from "../../../components/Header";
import Drawer from "../../../components/Drawer";
import Layout from "../../../components/Layout";
import Breadcrumb from "../../../components/Breadcrumb";
import {Grid} from "@material-ui/core";
import AddSettings from "../../../components/Dentist/profile/settings/AddSettings";
import AddWatermark from "../../../components/Dentist/profile/settings/AddWatermark";
import styled from "styled-components";
import AddPractice from "../../../components/Dentist/profile/settings/AddPractice";
import AddService from "../../../components/Dentist/profile/settings/AddService";

import {FlexWrapper, Box, MainContainer, FormBlockWrapper} from "../../../styles/Main.module";

const DentistSettingBlockWrapper = styled("div")`{
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  border-radius: 10px;
  padding: 20px;
`;

const CardDentist = ({}) => {
  return (
    <Layout title="Profile">
      <Box>
        <Header/>
        <FlexWrapper>
          <Drawer/>
          <MainContainer>
            <Breadcrumb point="Profile"/>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6} lg={6}>
                <FormBlockWrapper>
                  <AddSettings/>
                </FormBlockWrapper>
              </Grid>
              <Grid item xs={12} sm={6} lg={6}>
                <FormBlockWrapper>
                  <AddWatermark/>
                </FormBlockWrapper>
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6} lg={6}>
                <FormBlockWrapper>
                  <Grid container spacing={4}>
                    <Grid item xs={12} sm={12} lg={6}>
                      <DentistSettingBlockWrapper>
                        <AddPractice/>
                      </DentistSettingBlockWrapper>
                    </Grid>
                    <Grid item xs={12} sm={12} lg={6}>
                      <DentistSettingBlockWrapper>
                        <AddService/>
                      </DentistSettingBlockWrapper>
                    </Grid>
                  </Grid>
                </FormBlockWrapper>
              </Grid>
              <Grid item xs={12} sm={6} lg={6}>
              </Grid>
            </Grid>
          </MainContainer>
        </FlexWrapper>
      </Box>
    </Layout>
  );
};

export default CardDentist;

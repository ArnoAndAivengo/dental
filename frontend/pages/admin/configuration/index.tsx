import * as React from "react";
import {MeComponent} from "../../../generated/apolloComponents";
import Layout from "../../../components/Layout";
import Header from "../../../components/Header";
import Breadcrumb from "../../../components/Breadcrumb";
import AdminDrawer from "../../../components/AdminPanel/AdminDrawer";
import {Grid} from "@material-ui/core";
import styled from "styled-components";
import ServiceConfig from "../../../components/AdminPanel/configuration/ServiceConfig";

const InfoPanelWrapper = styled("div")`
  background: #FFFFFF 0 0 no-repeat padding-box;
  padding: 15px;
  border-radius: 10px;
  margin-bottom: 10px;
`;

const AdminUsers = ({}) => {
  return (
    <Layout title="Profile">
      <div className="box">
        <Header/>
        <section className="profile">
          <MeComponent>
            {({data, loading}) => {
              if (!data || loading || !data.me) {
                return null;
              }
              return (
                <AdminDrawer/>
              );
            }}
          </MeComponent>
          <div className="main">
            <Breadcrumb point="Dashboard"/>

            <InfoPanelWrapper>
              <Grid container alignItems="center" justify="space-between" spacing={2}>
                <Grid item xs={12} sm={6} lg={3} spacing={10}>
                  <ServiceConfig/>
                </Grid>
              </Grid>

                {/*<p className="super-input   space-between ">*/}
                {/*                <span>*/}
                {/*                    <span>Practice 1</span>*/}
                {/*                    <span className="padding">London</span>*/}
                {/*                </span>*/}
                {/*  <span className="delete-big">x </span>*/}
                {/*</p>*/}

            </InfoPanelWrapper>
          </div>
        </section>
      </div>
      <style jsx>{`
        * {
          box-sizing: border-box;
        }

        button:hover {
          box-shadow: 0 0 5px #ccc;
          cursor: pointer;
        }

        .flex-space-between {
          display: flex;
          justify-content: space-between;
          align-items: center;
        }

        .container {
          margin: 0 auto;
          width: 99%;
          box-shadow: 0 0 10px #ccc;
          background: #E7E7E7;
        }

        .box {
          max-width: 100%;
        }

        .topmenu {
          height: 80px;
          width: 100%;
          background: #B1B1B1;
          border: 1px solid #707070;
        }

        .profile {
          display: flex;
          flex-direction: row;
          flex-wrap: nowrap;

        }

        .main {
          width: 100%;
          padding: 15px;
          background: #F0F0F0 0% 0% no-repeat padding-box;
        }

        .h2 {
          text-align: left;
          font: normal normal normal 20px/27px Segoe UI;
          letter-spacing: 0px;
          color: #000000;
        }

        .h3 {
          text-align: left;
          font: normal normal normal 16px/21px Segoe UI;
          letter-spacing: 0px;
          color: #000000;
        }

        .text-form1 {
          text-align: left;
          font: normal normal normal 11px/15px Segoe UI;
          letter-spacing: 0px;
          color: #000000;
        }

        .text-form2 {
          text-align: left;
          font: normal normal normal 13px/17px Segoe UI;
          letter-spacing: 0px;
          color: #000000;
        }

        .block1 {
          max-width: 100%;
          max-height: 100%;
          background: transparent;
          border-radius: 10px;
          display: flex;
          flex-direction: row;
          flex-wrap: wrap;
          align-items: flex-start;
        }

        .block2 {
          width: 100%;
          background: #FFFFFF 0% 0% no-repeat padding-box;
          padding: 22px 10px;
          display: flex;
          flex-direction: row;
          flex-wrap: wrap;
          justify-content: space-evenly;
          align-items: flex-start;
        }

        .block1-form1,
        .block2-form1 {
          background: #FFFFFF 0% 0% no-repeat padding-box;
          width: 100%;
          border-radius: 10px;
          margin-bottom: 10px;
        }

        .block1-form2 {
          background: #FFFFFF 0% 0% no-repeat padding-box;
          display: flex;
          flex-direction: row;
          flex-wrap: nowrap;
          justify-content: space-between;
          align-items: flex-start;
          width: 100%;
          border-radius: 10px;
          margin-bottom: 10px;
        }

        .block2-form2 {
          margin-top: 98px;
          width: 100%;
          display: flex;
          flex-direction: column;
          justify-content: flex-start;
          align-items: flex-start;
        }

        .block2-form3 {
          margin-top: 80px;
          width: 100%;
          display: flex;
          flex-direction: row;
          justify-content: center;
          align-items: flex-start;
        }

        .column-center {
          border: 1px solid #707070;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
          align-self: stretch;
          width: 48%;
          padding: 15px;
          margin: 15px 0;
          text-align: center;
        }

        .column-between {
          border: 1px solid #707070;
          display: flex;
          flex-direction: column;
          justify-content: space-between;
          align-items: center;
          align-self: stretch;
          width: 48%;
          padding: 15px;
          margin: 15px 0;
          text-align: center;
        }

        .padding {
          padding: 15px;
        }

        .input1 {
          margin: 0 0 15px 0;
          width: 230px;
          height: 24px;
          background: #FFFFFF 0% 0% no-repeat padding-box;
          border: 1px solid #707070;
          padding-left: 10px;
          display: flex;
          align-items: center;
        }

        .input2 {
          margin: 0 0 15px 0;
          width: 176px;
          max-width: 100%;
          height: 26px;
          background: #FFFFFF 0% 0% no-repeat padding-box;
          border: 1px solid #707070;
          padding-left: 10px;
          display: flex;
          align-items: center;
        }


        .big-input {
          height: 51px;
        }

        .super-input {
          cursor: pointer;
          border-radius: 10px;
          margin-top: 0;
          margin-left: 10px;
          width: 328px;
          max-width: 100%;
          height: 28px;
          background: #FFFFFF 0% 0% no-repeat padding-box;
          border: 1px solid #0d9da6;
          padding-left: 10px;
          display: flex;
          align-items: center;
          text-align: left;
          font: normal normal normal 16px/21px Segoe UI;
          letter-spacing: 0px;
          color: #000;
        }

        .super-input:hover {
          background: #0d9da6;
          color: #fff;
        }

        .button-small {
          width: 55px;
          height: 19px;
          background: #DBDBDB 0% 0% no-repeat padding-box;
          border: 1px solid #707070;
          text-align: left;
          font: normal normal normal 6px/8px Segoe UI;
          letter-spacing: 0px;
          color: #000000;
          border-radius: 10px;
        }

        .button-medium {
          width: 91px;
          height: 18px;
          background: #FFFFFF 0% 0% no-repeat padding-box;
          border: 1px solid #707070;
          text-align: left;
          font: normal normal normal 7px/10px Segoe UI;
          letter-spacing: 0px;
          color: #000000;
          border-radius: 10px;
          display: flex;
          justify-content: center;
          align-items: center;
        }

        .button-big {
          /* width: 205px; */
          margin: 0 10px;
          padding-left: 20px;
          padding-right: 20px;
          height: 35px;
          background: #FFFFFF 0% 0% no-repeat padding-box;
          border: 1px solid #707070;
          border-radius: 20px;
          text-align: left;
          font: normal normal normal 14px/21px Segoe UI;
          letter-spacing: 0px;
          color: #000000;
          display: flex;
          justify-content: center;
          align-items: center;
        }

        .stripe {
          width: 229px;
          height: 0px;
          border: 1px solid #D5D5D5;

        }

        .big-square {
          width: 204px;
          height: 49px;
          background: #E1E1E1 0% 0% no-repeat padding-box;
          border: 1px solid #707070;
          text-align: left;
          font: normal normal normal 13px/17px Segoe UI;
          letter-spacing: 0px;
          color: #A2A2A2;
          display: flex;
          justify-content: center;
          align-items: center;
        }

        .fa-user-circle {
          margin: 0;
          font-size: 60px;
          color: #858585;
        }

        .space-between {
          display: flex;
          justify-content: space-between;
        }

        .delete {
          padding-right: 5px;
          font-weight: bold;
          font-size: 12px;
        }

        .delete-big {
          padding-right: 5px;
          font-weight: bold;
          font-size: 20px;
        }

        .border {
          border: 1px solid #707070;
        }

        .block50 {
          width: 48%;
        }

        .top-none {
          margin-top: 0;
          padding-top: 0;
        }

        .input-background {
          background: #D0D0D0;
        }

        .flex-start {
          display: flex;
          flex-direction: row;
          flex-wrap: nowrap;
          justify-content: flex-start;
        }

        .flex-center {
          display: flex;
          flex-direction: row;
          flex-wrap: nowrap;
          justify-content: center;
        }

        .button-gray {
          width: 133px;
          height: 34px;
          background: #DCDCDC 0% 0% no-repeat padding-box;
          border: 1px solid #707070;
          border-radius: 26px;
          display: flex;
          justify-content: center;
          align-items: center;
          margin: 10px;
        }

        .input-checkbox {
          margin-top: 0;
          padding-top: 0;
          width: 294px;
          height: 39px;
          background: #FFFFFF 0% 0% no-repeat padding-box;
          border: 1px solid #707070;
          text-align: left;
          font: normal normal normal 16px/21px Segoe UI;
          letter-spacing: 0px;
          color: #000000;
          display: flex;
          align-items: center;
          padding-left: 15px;
        }

        .fa-caret-down {
          font-size: 25px;
          padding-right: 10px;

        }

        #tab-active {
          color: #fff;
          background: #B0B0B0;
        }

        .block {
          max-width: 100%;
          min-width: 180px;
        }

        .search {

          height: 47px;
          border-radius: 30px;
          border: 1px solid #0d9da6;
          display: flex;
          flex-direction: row;
          align-items: center;
        }

        .search-button {
          width: 100%;
          cursor: pointer;
          background: #fff;
          height: 47px;
          border-radius: 30px;
          border: 1px solid #0d9da6;
          display: flex;
          flex-direction: row;
          justify-content: center;
          align-items: center;
          padding: 0 20px;
          color: #000;
        }

        .search-button:hover {
          background: #0d9da6;
          color: #fff;
        }

        .fa-search {
          padding-right: 20px;
          font-size: 30px;
        }

        .image-title {
          text-align: left;
          font: normal normal bold 24px/32px Segoe UI;
          letter-spacing: 0px;
          color: #000000;
        }

        .image-alt {
          text-align: left;
          font: normal normal normal 24px/32px Segoe UI;
          letter-spacing: 0px;
          color: #000000;
        }

        .image {
          width: 250px;
        }

        .water {
          position: relative;
          margin: 0;
          padding: 0;
        }

        .watermark {
          position: absolute;
          bottom: 10px;
          right: 10px;
          text-align: left;
          font: normal normal normal 13px/17px Segoe UI;
          letter-spacing: 0px;
          color: #E4E4E4;
        }

        .block-image {
          background: #fff;
          width: 250px;
          display: flex;
          flex-direction: column;
          justify-content: flex-start;
          align-items: flex-start;
          margin: 10px;
          transition: 0.3s linear;

        }

        .block-image:hover {
          cursor: pointer;
          box-shadow: 0 0 10px rgb(73, 73, 73);
        }

        .image-description {
          width: 100%;
          background: #fff;
        }

        @media (max-width: 1626px) {
          .image {
            width: 330px;
          }

          .block-image {
            width: 250px;
          }
        }

        @media (max-width: 1320px) {
          .block1 {
            flex-wrap: wrap;
            height: auto;
            justify-content: center;
          }
        }

        @media (max-width: 870px) {


        }

        @media (max-width: 774px) {
          .image {
            width: 600px;
            max-width: 100%;
          }

          .block-image {
            width: 600px;
            max-width: 100%;
          }
        }

        @media (max-width: 622px) {
          .image-description * {
            font-size: 1em;
          }

          .block1-form1,
          .block1-form2,
          .block2-form1 {
            flex-wrap: wrap;
          }

          .column-center,
          .column-between,
          .block,
          .input1,
          .block50 {
            width: 100%;
          }

          .image,
          .water {
            width: 100%;
          }

          .top-none {
            margin-top: 50px;
            width: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;
          }

          .input2 {
            width: 100%;
          }

          .h2 {
            text-align: center;
          }

          .block2-form2 {
            margin-top: 20px;
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: flex-start;
            align-items: center;
          }

          .block1,
          .block2 {
            width: 100%;
          }

          .column-center,
          .column-between,
          .block,
          .input1 {
            width: 100%;
          }
        }

        @media (max-width: 584px) {
          .flex-start {
            flex-wrap: wrap;
            justify-content: space-evenly;
          }


          .button-big {
            font-size: 12px;
            padding-left: 3px;
            padding-right: 3px;
          }
        }

        /* @media (max-width: 734px) {

            .block1,
            .block2 {
                width: 100%;
            }

            .column-center,
            .column-between,
            .block,
            .input1 {
                width: 100%;
            }
        } */

      `}</style>
    </Layout>
  )

}

export default AdminUsers;

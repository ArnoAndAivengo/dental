import * as React from "react";
import {MeComponent} from "../../../generated/apolloComponents";
import Layout from "../../../components/Layout";
import Header from "../../../components/Header";
import Breadcrumb from "../../../components/Breadcrumb";
import AdminDrawer from "../../../components/AdminPanel/AdminDrawer";
import PieGraph from "../../../components/AdminPanel/Dashboard/PieGraph";
import LineGraph from "../../../components/AdminPanel/Dashboard/LineGraph";
import {Grid} from "@material-ui/core";
import styled from "styled-components";

const InfoPanelWrapper = styled("div")`
  display: flex;
  //flex-direction: column;
  //flex-wrap: wrap;
  align-items: center;
  background: #FFFFFF 0 0 no-repeat padding-box;
  padding: 15px;
  border-radius: 10px;
  //margin-right: 10px;
  //width: 360px;
  margin-bottom: 10px;
`;

const InfoPanelBlock = styled("div")`
  display: flex;
  padding: 25px 15px;
  flex-wrap: nowrap;
  box-shadow: 0 0 10px #ccc;
  background: #E7E7E7;
  border-radius: 10px;
  margin-bottom: 30px;
`;

const AdminDashboard = ({}) => {
  return (
    <Layout title="Profile">
      <div className="box">
        <Header/>
        <section className="profile">
          <MeComponent>
            {({data, loading}) => {
              if (!data || loading || !data.me) {
                return null;
              }
              return (
                <AdminDrawer/>
              );
            }}
          </MeComponent>
          <div className="main">
            <Breadcrumb point="Dashboard"/>
            <InfoPanelWrapper>
              <Grid container alignItems="center" spacing={10}>
                <Grid item xs={12} sm={12} lg={5}>
                  <InfoPanelBlock>
                    <Grid container alignItems="center" justify="space-between" direction="row" style={{textAlign: 'center'}}>
                      <Grid item xs={4} sm={4} lg={4}>
                        <div style={{fontSize: 24}}>1,202</div>
                        <div>Unique Visitor</div>
                      </Grid>
                      <Grid item xs={4} sm={4} lg={4}>
                        <div style={{fontSize: 24}}>1,202</div>
                        <div>Unique Visitor</div>
                      </Grid>
                      <Grid item xs={4} sm={4} lg={4}>
                        <div style={{fontSize: 24}}>1,202</div>
                        <div>Unique Visitor</div>
                      </Grid>
                    </Grid>
                  </InfoPanelBlock>
                  <InfoPanelBlock>
                    <Grid container alignItems="center" justify="space-between" direction="row" style={{textAlign: 'center'}}>
                      <Grid item xs={4} sm={4} lg={4}>
                        <div style={{fontSize: 24}}>1,202</div>
                        <div>Unique Visitor</div>
                      </Grid>
                      <Grid item xs={4} sm={4} lg={4}>
                        <div style={{fontSize: 24}}>1,202</div>
                        <div>Unique Visitor</div>
                      </Grid>
                      <Grid item xs={4} sm={4} lg={4}>
                        <div style={{fontSize: 24}}>1,202</div>
                        <div>Unique Visitor</div>
                      </Grid>
                    </Grid>
                  </InfoPanelBlock>
                </Grid>
                <Grid item xs={12} sm={6} lg={3}>
                  <PieGraph title="Accounts Created / Deleted"/>
                </Grid>
                <Grid item xs={12} sm={6} lg={3}>
                  <PieGraph title="Subscribed Users"/>
                </Grid>
              </Grid>
            </InfoPanelWrapper>
            <InfoPanelWrapper>
              <Grid container alignItems="center" spacing={10}>
                <Grid item xs={12} sm={5} lg={6}>
                  <LineGraph title="Monthly Renenue"/>
                </Grid>
                <Grid item xs={12} sm={5} lg={6}>
                  <LineGraph title="Monthly Renenue"/>
                </Grid>
              </Grid>
            </InfoPanelWrapper>
          </div>
        </section>
      </div>
      <style jsx>{`

        * {
          box-sizing: border-box;

        }

        body {
          margin: 0;
          padding: 0;
        }

        .container {
          margin: 0 auto;
          width: 99%;
          box-shadow: 0 0 10px #ccc;
          background: #E7E7E7;
        }

        .box {
          max-width: 100%;
        }

        .profile {
          display: flex;
          flex-direction: row;
          flex-wrap: nowrap;
        }

        .main {
          width: 100%;
          padding: 10px;
          background: #F0F0F0 0% 0% no-repeat padding-box;
        }

        .block1 {
          /* width: 650px; */
          max-width: 100%;
          max-height: 100%;
          background: transparent;
          border-radius: 10px;
          display: flex;
          flex-direction: row;
          flex-wrap: wrap;
          align-items: flex-start;
        }

        .block2 {
          /* width: 650px; */
          max-width: 100%;
          height: 900px;
          max-height: 100%;
          background: #FFFFFF 0% 0% no-repeat padding-box;
          border: 1px solid #707070;
          margin: 60px 20px 60px 20px;
          padding: 40px;
        }

        .center {
          display: flex;
          flex-direction: row;
          justify-content: center;
          align-items: center;
          flex-wrap: wrap;
        }

        .left {
          display: flex;
          flex-direction: row;
          justify-content: flex-start;
          align-items: center;
          flex-wrap: wrap;
          align-self: stretch;
        }

        .right {
          display: flex;
          flex-direction: row;
          justify-content: flex-end;
          align-items: center;
          flex-wrap: wrap;
          align-self: stretch;
        }

        .between {
          display: flex;
          flex-direction: row;
          justify-content: space-between;
          align-items: center;
          flex-wrap: wrap;
          align-self: stretch;
        }

        .column {
          display: flex;
          flex-direction: column;
          justify-content: flex-start;
          align-items: flex-start;
          align-self: stretch;
        }

        .margin-top {
          margin-top: 40px;
        }

        .margin-top-big {
          margin-top: 80px;
        }

        .margin-bottom {
          margin-bottom: 40px;
        }

        .section1 {
          display: flex;
          flex-direction: row;
          justify-content: space-between;
          align-items: center;
          flex-wrap: nowrap;
        }

        .section1 div,
        .section1 div div {
          width: 363px;
          min-width: 250px;
          /* max-width: 100%; */
          /* height: 363px; */
          margin: 0 30px 0 0;
        }

        .section1 div div {
          max-width: 100%;
        }

        .section2 {
          display: flex;
          flex-direction: row;
          justify-content: left;
          align-items: center;
          flex-wrap: wrap;
          align-self: stretch;
        }

        .section2 div {

          width: 349px;
          height: 346px;
          margin: 5px 10px 5px 0;
        }

        .h2 {
          text-align: left;
          font: normal normal bold 21px/28px Segoe UI;
          letter-spacing: 1.31px;
          color: #333333;
          opacity: 1;
        }

        .strong {
          text-align: left;
          font: normal normal bold 16px/21px Segoe UI;
          letter-spacing: 0px;
          color: #000000;
          opacity: 1;
          margin-bottom: 0px;
        }

        .strong2 {
          text-align: left;
          font: normal normal bold 14px/16px Segoe UI;
          letter-spacing: 0px;
          color: #000000;
          opacity: 1;
        }

        .image {
          width: 363px;
          max-width: 100%;
          /* max-width: 100%; */
          /* height: 369px; */
        }

        .margin {
          margin-bottom: 10px;
        }

        .start {
          align-items: flex-start;
          justify-content: flex-start;
        }

        .end {
          align-items: flex-start;
          justify-content: flex-end;
        }

        .center2 {
          align-items: flex-start;
          justify-content: center;
        }

        .nowrap {
          flex-wrap: nowrap;
        }

        .text {
          padding-left: 20px;
          text-align: left;
          font: normal normal normal 14px/19px Segoe UI;
          letter-spacing: 0.88px;
          color: #333333;
          opacity: 1;
        }

        .little-text {
          text-align: left;
          font: normal normal normal 12px/16px Segoe UI;
          letter-spacing: 0px;
          color: #000000;
          opacity: 1;
        }

        .button1 {
          background: #F2F2F2 0% 0% no-repeat padding-box;
          border: 1px solid #DADADA;
          border-radius: 22px;
          opacity: 1;
        }

        .block {
          position: relative;
        }

        .block-button {
          position: absolute;
          top: 8px;
          left: 6px;
          width: 98px;
          height: 24px;
          background: #FFFFFF 0% 0% no-repeat padding-box;
          border: 1px solid #707070;
          border-radius: 20px;
          opacity: .7;

        }

        .block-button-text {
          text-align: left;
          font: normal normal normal 8px/10px Segoe UI;
          letter-spacing: 0.44px;
          color: #333333;
          opacity: 1;
        }

        .block-watermark {
          position: absolute;
          bottom: -5px;
          right: 20px;
          width: 107px;
          height: 31px;
          text-align: left;
          font: normal normal normal 23px/31px Segoe UI;
          letter-spacing: 0px;
          color: #FFFFFF80;
          opacity: 1;
        }

        .center-vertical {
          align-items: center;
        }

        .topmenu {
          height: 90px;
          display: flex;
          justify-content: flex-start;
          align-items: center;
        }

        .menu-mobile {
          width: 50px;
          height: 37px;
          border-top: 2px solid #fff;
          border-bottom: 2px solid #fff;
          color: #fff;
          font-size: 12px;

        }

        .material-icons {
          color: #fff;
          font-size: 56px;
        }

        .footer {
          position: relative;
          height: 100px;
          background: #ccc;
          margin-top: 100px;
        }

        .border-left {
          border-left: 1px solid #C7C7C7;
          padding-left: 30px;
        }

        .show-desctop {
          display: block;
        }

        .show-mobile {
          display: none;
        }

        .image2 {
          max-width: 100%;
          flex-grow: 1;
          align-self: stretch;
        }

        @media (max-width: 1104px) {
          .section2 {
            justify-content: space-evenly;
          }
        }

        @media (max-width: 840px) {
          .margin-top-big {
            margin-top: 20px;
          }

          .section1 {
            flex-wrap: nowrap;
          }

          .show-desctop {
            display: none;
          }

          .show-mobile {
            display: block;
            justify-content: flex-start;
            align-self: stretch;
            flex-grow: 1;
            margin: 0;
            min-width: 280px;
          }

          .border-left {
            border: 1px solid #C7C7C7;
            padding: 5px;

          }

          .image,
          .section1 div {
            max-width: 100%;
          }

          .square {
            max-width: 100%;
            width: 100%;
          }

        }

        @media (max-width: 630px) {

        }

      `}</style>
    </Layout>
  )

}

export default AdminDashboard;

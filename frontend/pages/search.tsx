import React, {useEffect, useState} from "react";
import Layout from "../components/Layout";
import {MeComponent, useQuerySearchDentistsQuery} from "../generated/apolloComponents";
import {Grid, IconButton, Input, Slider, Typography} from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';
import {DistanceWrapper, InputSearch, Main, Profile, SearchBlock, SearchPanelWrapper} from "../styles/Search.module";
import Header from "../components/Header";
import Services from "../components/Search/Services";
import GoogleMapReactComponent from "../components/Search/GoogleMapReact";
import Drawer from "../components/Drawer";
import {convertCityCoords} from "../utils/search/converCityCoords";
import CardDentist from "../components/Search/CardDentist";

const Search = () => {
  const [currentDentist, setCurrentDentist] = useState(undefined);
  const [searchDentists, setSearchDentists] = useState(undefined);
  const [ipCoords, setIpCoords] = useState(undefined);
  const [searchDentistsLocations, setSearchDentistsLocations] = useState(undefined);
  const [searchCoords, setSearchCoords]: any = useState();
  let searchValue = ''
  const [valueSlider, setValueSlider] = React.useState<number | string | Array<number | string>>(10);
  const {data, loading}: any = useQuerySearchDentistsQuery({
    variables: {
      lat: searchCoords?.lat,
      lng: searchCoords?.lng
    },
  });
  const [dentists, setDentists] = useState(undefined);
  useEffect(() => {
    if (!loading && data) {
      setSearchDentists(data.querySearchDentist)
      setDentists(data.querySearchDentist)
      setSearchDentistsLocations(data.querySearchDentist)
    }
  }, [loading, data])

  if (!ipCoords) {
    convertCityCoords().then((result) => {
      setIpCoords(result)
      if (!searchCoords) {
        setSearchCoords(result)
      }
    })
  }

  const enterKeyDown = (e: { keyCode: number; }) => {
    if (e.keyCode === 13) {
      changeSearch()
    }
  }

  const handleBlur = () => {
    if (valueSlider < 0) {
      setValueSlider(0);
    } else if (valueSlider > 100) {
      setValueSlider(100);
    }
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValueSlider(event.target.value === '' ? '' : Number(event.target.value));
  };

  const getDentist = (dentists: any) => {
    setDentists(dentists)
    setSearchDentists(dentists)
  }

  const findCoordinatesDentists = (coordinate: any, distance: number, dentists: []): object | [] => {
    let distanceDent: any[] = [];

    if (!dentists) {
      return {};
    }

    dentists.map((dent: { lng: any; lat: any; }) => {
      const a = {'Longitude': coordinate.lng, 'Latitude': coordinate.lat};
      const b = {'Longitude': dent.lng, 'Latitude': dent.lat};
      const distanceCur = (111.111 *
        (180 / Math.PI) * (
          Math.acos(Math.cos(a.Latitude * (Math.PI / 180))
            * Math.cos(b.Latitude * (Math.PI / 180))
            * Math.cos((a.Longitude - b.Longitude) * (Math.PI / 180))
            + Math.sin(a.Latitude * (Math.PI / 180))
            * Math.sin(b.Latitude * (Math.PI / 180)))))
      if (distanceCur < distance) {
        distanceDent.push(dent)
      }
    })
    return distanceDent
  }

  const changeSearch = () => {
    fetch('https://maps.google.com/maps/api/geocode/json?sensor=false&address=' + searchValue + '&key=AIzaSyDMYrZZhMGlK5PKOMQRQMVffXnUJwgyatY')
      .then(response => response.json())
      .then(result => {
        setSearchCoords(result.results[0].geometry.location)
        getDistance({}, valueSlider, result.results[0].geometry.location, searchDentists)
      })
      .catch((_error: any) => {
      })
  }

  const getDistance = (_event: any, newValue: any, coordinate?: object | undefined, searchDent?: object) => {
    setValueSlider(newValue)
    let searchD: any = [];
    let coordinates: any = {};

    if (searchDent) {
      searchD = searchDent
    } else {
      searchD = searchDentists
    }

    if (coordinate) {
      coordinates = coordinate
    } else {
      coordinates = searchCoords
    }

    if (!searchD) {
      return
    }
    const findCoordinatesDent: any = findCoordinatesDentists(coordinates, newValue, searchD)

    setDentists(findCoordinatesDent)
  }

  return (
    <Layout title="Search page">
      <Header/>
      <Profile>
        <MeComponent>
          {({data, loading}) => {
            if (!data || loading || !data.me) {
              return null;
            }
            return (
              <Drawer/>
            );
          }}
        </MeComponent>
        <Main>
          <SearchPanelWrapper>
            <Grid container alignItems="center" justify="space-between" spacing={2}>
              <Grid item xs={12} sm={6} lg={3}>
                <SearchBlock>
                  <IconButton
                    onClick={changeSearch}
                    aria-label="search"
                    style={{width: '32px', height: '32px', zoom: 1.6, color: '#0d9da6'}}>
                    <SearchIcon/>
                  </IconButton>
                  <InputSearch
                    placeholder="Search Google Maps"
                    onChange={e => searchValue = e.target.value}
                    onKeyDown={enterKeyDown}
                  />
                </SearchBlock>
              </Grid>
              <Grid item xs={12} sm={6} lg={3}>
                <Services
                  getDentist={getDentist}
                  dentists={searchDentists}
                  searchDentistsLocations={searchDentistsLocations}
                  searchCoords={searchCoords}
                />
              </Grid>
              <Grid item xs={12} lg={3}>
                <DistanceWrapper>
                  <Typography id="discrete-slider" gutterBottom>
                    Distance
                  </Typography>
                  <Grid container spacing={2} alignItems="center">
                    <Grid item xs>
                      <Slider
                        value={typeof valueSlider === 'number' ? valueSlider : 0}
                        aria-labelledby="discrete-slider"
                        // @ts-ignore
                        onChange={getDistance}
                      />
                    </Grid>
                    <Grid item xs>
                      <Input
                        value={valueSlider}
                        margin="dense"
                        onChange={handleInputChange}
                        onBlur={handleBlur}
                        inputProps={{
                          step: 1,
                          min: 0,
                          max: 100,
                          type: 'number',
                          'aria-labelledby': 'input-slider',
                        }}
                      />
                    </Grid>
                  </Grid>
                </DistanceWrapper>
              </Grid>
              <Grid item lg={2}>
              </Grid>
            </Grid>
          </SearchPanelWrapper>
          <MeComponent>
            {({data}: any) => {
              return (
                <GoogleMapReactComponent
                  dentists={dentists}
                  me={data?.me}
                  currentDentist={currentDentist}
                  searchCoords={searchCoords}
                  ipCoords={ipCoords}
                />
              );
            }}
          </MeComponent>
          <CardDentist dentists={dentists} setCurrentDentist={setCurrentDentist}/>
        </Main>
      </Profile>
    </Layout>
  )
};

export default Search;
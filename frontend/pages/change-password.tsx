import {Field, Formik} from "formik";
// @ts-ignore
import {NextContext} from "next";
import Router from "next/router";
import React from "react";
import {InputField} from "../components/Form/fields/InputField";
import Layout from "../components/Layout";
import {useChangePasswordMutation} from "../generated/apolloComponents";

const ChangePassword = ({token}: { token: string }) => {
  return (
    <Layout title="Change Password page">

      <Formik
        onSubmit={async data => {
          await useChangePasswordMutation({
            variables: {
              data: {
                password: data.password,
                token
              }
            }
          });
          Router.push("/");
        }}
        initialValues={{
          password: ""
        }}
      >
        {({handleSubmit}) => (
          <form onSubmit={handleSubmit}>
            <Field
              name="password"
              placeholder="password"
              component={InputField}
              type="password"
            />
            <button type="submit">change password</button>
          </form>
        )}
      </Formik>
    </Layout>
  );
};

ChangePassword.getInitialProps = ({
    query: {token}
  }: NextContext<{ token: string }>) => {
  return {
    token
  };
};

export default ChangePassword;

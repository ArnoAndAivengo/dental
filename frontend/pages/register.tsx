import {Field, Formik} from "formik";
import Router from "next/router";
import React from "react";
import {InputField} from "../components/Form/fields/InputField";
import Layout from "../components/Layout";
import Avatar from "../components/Form/Avatar";
import Typography from "../components/Typography";
import FooterForm from "../components/FooterForm";
import {RegisterComponent} from "../generated/apolloComponents";
import {Image, RightSide} from "../styles/Main.module";
import ButtonForm from "../components/Buttons/ButtonForm";
import {Grid} from "@material-ui/core";
import Link from "next/link";

const Register = () => {
  return (
    <Layout title="Register page">
      <Grid container>
        <Grid item xs={6} sm={6} lg={6}>
          <Image/>
        </Grid>
        <Grid item xs={6} sm={6} lg={6}>
          <RightSide>
            <Avatar/>
            <Typography title='Register'/>
            <RegisterComponent>
              {register => (
                <Formik
                  validateOnBlur={false}
                  validateOnChange={false}
                  onSubmit={async (data, {setErrors}) => {
                    try {
                      const response: any = await register({
                        variables: {
                          data
                        }
                      });
                      const URL: string = "http://localhost:4000/uploads-file/" + response.data.register.id + "/avatar/"
                      const requestOptions: {} = {
                        method: 'POST',
                        redirect: 'follow'
                      };

                      await fetch(URL, requestOptions)
                      await Router.push("/check-email");
                    } catch (err) {
                      const errors: { [key: string]: string } = {};
                      err.graphQLErrors[0].validationErrors.forEach(
                        (validationErr: any) => {
                          Object.values(validationErr.constraints).forEach(
                            (message: any) => {
                              errors[validationErr.property] = message;
                            }
                          );
                        }
                      );
                      setErrors(errors);
                    }
                  }}
                  initialValues={{
                    email: "",
                    firstName: "",
                    lastName: "",
                    password: ""
                  }}
                >
                  {({handleSubmit}) => (
                    <form onSubmit={handleSubmit}>
                      <Field
                        name="firstName"
                        placeholder="firstName"
                        component={InputField}
                      />
                      <Field
                        name="lastName"
                        placeholder="lastName"
                        component={InputField}
                      />
                      <Field
                        name="email"
                        placeholder="email"
                        component={InputField}
                      />
                      <Field
                        name="password"
                        placeholder="password"
                        type="password"
                        component={InputField}
                      />
                      <ButtonForm title='Sign In'>submit</ButtonForm>
                    </form>
                  )}
                </Formik>
              )}
            </RegisterComponent>
            <Link href={"../login"}>Login your Dental account</Link>
            <Link href={"../search"}>Go to search Dentist</Link>
            <FooterForm/>
          </RightSide>
        </Grid>
      </Grid>
    </Layout>
  );
};

export default Register

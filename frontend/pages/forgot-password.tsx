import { Field, Formik } from "formik";
import Router from "next/router";
import React from "react";
import { InputField } from "../components/Form/fields/InputField";
import Layout from "../components/Layout";
import { ForgotPasswordComponent } from "../generated/apolloComponents";

const forgotPassword = () => {
  return (
    <Layout title="Forgot Password page">
      <ForgotPasswordComponent>
        {forgotPassword => (
          <Formik
            onSubmit={async data => {
              await forgotPassword({
                variables: data
              });
              Router.push("/check-email");
            }}
            initialValues={{
              email: ""
            }}
          >
            {({ handleSubmit }) => (
              <form onSubmit={handleSubmit}>
                <Field
                  name="email"
                  placeholder="email"
                  component={InputField}
                />
                <button type="submit">forgot password</button>
              </form>
            )}
          </Formik>
        )}
      </ForgotPasswordComponent>
    </Layout>
  );
};

export default forgotPassword;

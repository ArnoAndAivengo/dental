const withTypescript = require('@zeit/next-typescript')
module.exports = withTypescript()
module.exports = {
  typescript: {
    // !! WARN !!
    // Dangerously allow production builds to successfully complete even if
    // your project has type errors.
    // !! WARN !!
    ignoreBuildErrors: true,
  },
}

module.exports = {
  async redirects() {
    return [
      {
        source: '/team',
        destination: '/about',
        permanent: false,
      },
    ];
  },
};
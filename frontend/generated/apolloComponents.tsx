import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
import * as React from 'react';
import * as ApolloReactComponents from '@apollo/client/react/components';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The javascript `Date` as string. Type represents date and time as the ISO Date string. */
  DateTime: any;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};

export type ChangePasswordInput = {
  password: Scalars['String'];
  token: Scalars['String'];
};

export type Clinic = {
  __typename?: 'Clinic';
  id: Scalars['ID'];
  name: Scalars['String'];
  dentists: Array<Dentist>;
};

export type Coordinate = {
  __typename?: 'Coordinate';
  id: Scalars['ID'];
  type?: Maybe<Scalars['String']>;
  lat: Scalars['Float'];
  lng: Scalars['Float'];
  dentistConnection: Array<Location>;
};


export type Dentist = {
  __typename?: 'Dentist';
  id: Scalars['ID'];
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  qualifications?: Maybe<Scalars['String']>;
  bio?: Maybe<Scalars['String']>;
  website?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  postIndex?: Maybe<Scalars['String']>;
  email: Scalars['String'];
  practiceName?: Maybe<Scalars['String']>;
  lat: Scalars['Float'];
  lng: Scalars['Float'];
  registrationDate: Scalars['DateTime'];
  name: Scalars['String'];
  address: Scalars['String'];
  services: Array<ServiceToUser>;
  practices: Array<PracticeToUser>;
};

export type DentistProfileInput = {
  id: Scalars['ID'];
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  qualifications?: Maybe<Scalars['String']>;
  bio?: Maybe<Scalars['String']>;
  website?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  street?: Maybe<Scalars['String']>;
  postIndex?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
};

export type I2d4158ubkw9 = {
  password: Scalars['String'];
};

export type Istwdw3y2jnc = {
  password: Scalars['String'];
};

export type Location = {
  __typename?: 'Location';
  dentist: Dentist;
};

export type Mutation = {
  __typename?: 'Mutation';
  createClinic: Clinic;
  addDentistClinic: Scalars['Boolean'];
  deleteClinic: Scalars['Boolean'];
  createDentist: Dentist;
  addDentistSettings: Dentist;
  createService: Scalars['Boolean'];
  addDentistService: Scalars['Boolean'];
  deleteService: Scalars['Boolean'];
  createCoordinates: Coordinate;
  addDentistCoordinates: Scalars['Boolean'];
  changePassword?: Maybe<Dentist>;
  confirmDentist: Scalars['Boolean'];
  createPractice: Scalars['Boolean'];
  addDentistPractice: Scalars['Boolean'];
  deletePractice: Scalars['Boolean'];
  forgotPassword: Scalars['Boolean'];
  login?: Maybe<Dentist>;
  logout: Scalars['Boolean'];
  addProfilePicture: Scalars['Boolean'];
  register: Dentist;
};


export type MutationCreateClinicArgs = {
  name: Scalars['String'];
};


export type MutationAddDentistClinicArgs = {
  clinicId: Scalars['Int'];
  dentistId: Scalars['Int'];
};


export type MutationDeleteClinicArgs = {
  clinicId: Scalars['Int'];
};


export type MutationCreateDentistArgs = {
  data: RegisterInput;
};


export type MutationAddDentistSettingsArgs = {
  data: DentistProfileInput;
};


export type MutationCreateServiceArgs = {
  dentistId: Scalars['Int'];
  service: Scalars['String'];
};


export type MutationAddDentistServiceArgs = {
  serviceId: Scalars['Int'];
  dentistId: Scalars['Int'];
};


export type MutationDeleteServiceArgs = {
  serviceId: Scalars['Int'];
};


export type MutationCreateCoordinatesArgs = {
  lng: Scalars['Float'];
  lat: Scalars['Float'];
};


export type MutationAddDentistCoordinatesArgs = {
  coordinatesId: Scalars['Int'];
  dentistId: Scalars['Int'];
};


export type MutationChangePasswordArgs = {
  data: ChangePasswordInput;
};


export type MutationConfirmDentistArgs = {
  token: Scalars['String'];
};


export type MutationCreatePracticeArgs = {
  dentistId: Scalars['Int'];
  practice: Scalars['String'];
};


export type MutationAddDentistPracticeArgs = {
  practiceId: Scalars['Int'];
  dentistId: Scalars['Int'];
};


export type MutationDeletePracticeArgs = {
  practiceId: Scalars['Int'];
};


export type MutationForgotPasswordArgs = {
  email: Scalars['String'];
};


export type MutationLoginArgs = {
  password: Scalars['String'];
  email: Scalars['String'];
};


export type MutationAddProfilePictureArgs = {
  picture: Scalars['Upload'];
};


export type MutationRegisterArgs = {
  data: RegisterInput;
};

export type Practice = {
  __typename?: 'Practice';
  id: Scalars['ID'];
  name: Scalars['String'];
  dentists: Array<PracticeToUser>;
};

export type PracticeToUser = {
  __typename?: 'PracticeToUser';
  dentist: Dentist;
  practice: Practice;
};

export type Query = {
  __typename?: 'Query';
  clinics: Array<Clinic>;
  service: Array<Service>;
  dentists: Array<Dentist>;
  dentist?: Maybe<Dentist>;
  services: Array<Service>;
  coordinates: Array<Coordinate>;
  querySearchDentist: Array<Dentist>;
  practices: Array<Practice>;
  practice: Array<Practice>;
  me?: Maybe<Dentist>;
};


export type QueryServiceArgs = {
  service: Scalars['String'];
};


export type QueryDentistArgs = {
  id: Scalars['Int'];
};


export type QueryQuerySearchDentistArgs = {
  lng: Scalars['Float'];
  lat: Scalars['Float'];
};


export type QueryPracticeArgs = {
  practice: Scalars['String'];
};

export type RegisterInput = {
  password: Scalars['String'];
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  email: Scalars['String'];
};

export type Service = {
  __typename?: 'Service';
  id: Scalars['ID'];
  name: Scalars['String'];
  dentists: Array<ServiceToUser>;
};

export type ServiceToUser = {
  __typename?: 'ServiceToUser';
  dentist: Dentist;
  service: Service;
};


export type AddDentistPracticeMutationVariables = Exact<{
  dentistId: Scalars['Int'];
  practiceId: Scalars['Int'];
}>;


export type AddDentistPracticeMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'addDentistPractice'>
);

export type AddDentistSettingsMutationVariables = Exact<{
  data: DentistProfileInput;
}>;


export type AddDentistSettingsMutation = (
  { __typename?: 'Mutation' }
  & { addDentistSettings: (
    { __typename?: 'Dentist' }
    & Pick<Dentist, 'id' | 'name' | 'firstName' | 'lastName' | 'email' | 'postIndex' | 'city' | 'street' | 'phone' | 'website' | 'qualifications' | 'bio' | 'lat' | 'lng'>
  ) }
);

export type CreatePracticeMutationVariables = Exact<{
  practice: Scalars['String'];
  dentistId: Scalars['Int'];
}>;


export type CreatePracticeMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'createPractice'>
);

export type CreateServiceMutationVariables = Exact<{
  service: Scalars['String'];
  dentistId: Scalars['Int'];
}>;


export type CreateServiceMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'createService'>
);

export type ChangePasswordMutationVariables = Exact<{
  data: ChangePasswordInput;
}>;


export type ChangePasswordMutation = (
  { __typename?: 'Mutation' }
  & { changePassword?: Maybe<(
    { __typename?: 'Dentist' }
    & Pick<Dentist, 'id' | 'firstName' | 'lastName' | 'email' | 'name'>
  )> }
);

export type ConfirmDentistMutationVariables = Exact<{
  token: Scalars['String'];
}>;


export type ConfirmDentistMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'confirmDentist'>
);

export type DeletePracticeMutationVariables = Exact<{
  practiceId: Scalars['Int'];
}>;


export type DeletePracticeMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'deletePractice'>
);

export type DeleteServiceMutationVariables = Exact<{
  serviceId: Scalars['Int'];
}>;


export type DeleteServiceMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'deleteService'>
);

export type ForgotPasswordMutationVariables = Exact<{
  email: Scalars['String'];
}>;


export type ForgotPasswordMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'forgotPassword'>
);

export type LoginMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginMutation = (
  { __typename?: 'Mutation' }
  & { login?: Maybe<(
    { __typename?: 'Dentist' }
    & Pick<Dentist, 'id' | 'firstName' | 'lastName' | 'email' | 'name'>
  )> }
);

export type LogoutMutationVariables = Exact<{ [key: string]: never; }>;


export type LogoutMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'logout'>
);

export type RegisterMutationVariables = Exact<{
  data: RegisterInput;
}>;


export type RegisterMutation = (
  { __typename?: 'Mutation' }
  & { register: (
    { __typename?: 'Dentist' }
    & Pick<Dentist, 'id' | 'firstName' | 'lastName' | 'email' | 'name'>
  ) }
);

export type AddProfilePictureMutationVariables = Exact<{
  picture: Scalars['Upload'];
}>;


export type AddProfilePictureMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'addProfilePicture'>
);

export type DentistsQueryVariables = Exact<{ [key: string]: never; }>;


export type DentistsQuery = (
  { __typename?: 'Query' }
  & { dentists: Array<(
    { __typename?: 'Dentist' }
    & Pick<Dentist, 'id' | 'postIndex' | 'street' | 'bio' | 'city' | 'phone' | 'email' | 'firstName' | 'lastName' | 'name' | 'website' | 'qualifications' | 'address' | 'lat' | 'lng'>
    & { practices: Array<(
      { __typename?: 'PracticeToUser' }
      & { practice: (
        { __typename?: 'Practice' }
        & Pick<Practice, 'id' | 'name'>
      ) }
    )>, services: Array<(
      { __typename?: 'ServiceToUser' }
      & { service: (
        { __typename?: 'Service' }
        & Pick<Service, 'id' | 'name'>
      ) }
    )> }
  )> }
);

export type DentistQueryVariables = Exact<{
  id: Scalars['Int'];
}>;


export type DentistQuery = (
  { __typename?: 'Query' }
  & { dentist?: Maybe<(
    { __typename?: 'Dentist' }
    & Pick<Dentist, 'id' | 'postIndex' | 'street' | 'bio' | 'city' | 'phone' | 'email' | 'firstName' | 'lastName' | 'name' | 'website' | 'qualifications' | 'address' | 'lat' | 'lng'>
    & { practices: Array<(
      { __typename?: 'PracticeToUser' }
      & { practice: (
        { __typename?: 'Practice' }
        & Pick<Practice, 'id' | 'name'>
      ) }
    )>, services: Array<(
      { __typename?: 'ServiceToUser' }
      & { service: (
        { __typename?: 'Service' }
        & Pick<Service, 'id' | 'name'>
      ) }
    )> }
  )> }
);

export type CoordinatesQueryVariables = Exact<{ [key: string]: never; }>;


export type CoordinatesQuery = (
  { __typename?: 'Query' }
  & { coordinates: Array<(
    { __typename?: 'Coordinate' }
    & Pick<Coordinate, 'id' | 'lat' | 'lng'>
    & { dentistConnection: Array<(
      { __typename?: 'Location' }
      & { dentist: (
        { __typename?: 'Dentist' }
        & Pick<Dentist, 'id' | 'name'>
      ) }
    )> }
  )> }
);

export type MeQueryVariables = Exact<{ [key: string]: never; }>;


export type MeQuery = (
  { __typename?: 'Query' }
  & { me?: Maybe<(
    { __typename?: 'Dentist' }
    & Pick<Dentist, 'id' | 'postIndex' | 'street' | 'bio' | 'city' | 'phone' | 'email' | 'firstName' | 'lastName' | 'name' | 'website' | 'qualifications' | 'address' | 'lat' | 'lng'>
    & { practices: Array<(
      { __typename?: 'PracticeToUser' }
      & { practice: (
        { __typename?: 'Practice' }
        & Pick<Practice, 'id' | 'name'>
      ) }
    )>, services: Array<(
      { __typename?: 'ServiceToUser' }
      & { service: (
        { __typename?: 'Service' }
        & Pick<Service, 'id' | 'name'>
      ) }
    )> }
  )> }
);

export type PracticesQueryVariables = Exact<{ [key: string]: never; }>;


export type PracticesQuery = (
  { __typename?: 'Query' }
  & { practices: Array<(
    { __typename?: 'Practice' }
    & Pick<Practice, 'id' | 'name'>
    & { dentists: Array<(
      { __typename?: 'PracticeToUser' }
      & { dentist: (
        { __typename?: 'Dentist' }
        & Pick<Dentist, 'id' | 'postIndex' | 'street' | 'bio' | 'city' | 'phone' | 'email' | 'firstName' | 'lastName' | 'name' | 'website' | 'qualifications' | 'lat' | 'lng'>
      ) }
    )> }
  )> }
);

export type PracticeQueryVariables = Exact<{
  practice: Scalars['String'];
}>;


export type PracticeQuery = (
  { __typename?: 'Query' }
  & { practice: Array<(
    { __typename?: 'Practice' }
    & Pick<Practice, 'id' | 'name'>
    & { dentists: Array<(
      { __typename?: 'PracticeToUser' }
      & { dentist: (
        { __typename?: 'Dentist' }
        & Pick<Dentist, 'id' | 'postIndex' | 'street' | 'bio' | 'city' | 'phone' | 'email' | 'firstName' | 'lastName' | 'name' | 'website' | 'qualifications' | 'lat' | 'lng'>
      ) }
    )> }
  )> }
);

export type QuerySearchDentistsQueryVariables = Exact<{
  lat: Scalars['Float'];
  lng: Scalars['Float'];
}>;


export type QuerySearchDentistsQuery = (
  { __typename?: 'Query' }
  & { querySearchDentist: Array<(
    { __typename?: 'Dentist' }
    & Pick<Dentist, 'id' | 'postIndex' | 'street' | 'bio' | 'city' | 'phone' | 'email' | 'firstName' | 'lastName' | 'name' | 'website' | 'qualifications' | 'address' | 'lat' | 'lng'>
  )> }
);

export type ServicesQueryVariables = Exact<{ [key: string]: never; }>;


export type ServicesQuery = (
  { __typename?: 'Query' }
  & { services: Array<(
    { __typename?: 'Service' }
    & Pick<Service, 'id' | 'name'>
    & { dentists: Array<(
      { __typename?: 'ServiceToUser' }
      & { dentist: (
        { __typename?: 'Dentist' }
        & Pick<Dentist, 'id' | 'postIndex' | 'street' | 'bio' | 'city' | 'phone' | 'email' | 'firstName' | 'lastName' | 'name' | 'website' | 'qualifications' | 'lat' | 'lng'>
      ) }
    )> }
  )> }
);

export type ServiceQueryVariables = Exact<{
  service: Scalars['String'];
}>;


export type ServiceQuery = (
  { __typename?: 'Query' }
  & { service: Array<(
    { __typename?: 'Service' }
    & Pick<Service, 'id' | 'name'>
    & { dentists: Array<(
      { __typename?: 'ServiceToUser' }
      & { dentist: (
        { __typename?: 'Dentist' }
        & Pick<Dentist, 'id' | 'postIndex' | 'street' | 'bio' | 'city' | 'phone' | 'email' | 'firstName' | 'lastName' | 'name' | 'website' | 'qualifications' | 'lat' | 'lng'>
      ) }
    )> }
  )> }
);


export const AddDentistPracticeDocument = gql`
    mutation AddDentistPractice($dentistId: Int!, $practiceId: Int!) {
  addDentistPractice(dentistId: $dentistId, practiceId: $practiceId)
}
    `;
export type AddDentistPracticeMutationFn = Apollo.MutationFunction<AddDentistPracticeMutation, AddDentistPracticeMutationVariables>;
export type AddDentistPracticeComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<AddDentistPracticeMutation, AddDentistPracticeMutationVariables>, 'mutation'>;

    export const AddDentistPracticeComponent = (props: AddDentistPracticeComponentProps) => (
      <ApolloReactComponents.Mutation<AddDentistPracticeMutation, AddDentistPracticeMutationVariables> mutation={AddDentistPracticeDocument} {...props} />
    );
    

/**
 * __useAddDentistPracticeMutation__
 *
 * To run a mutation, you first call `useAddDentistPracticeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddDentistPracticeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addDentistPracticeMutation, { data, loading, error }] = useAddDentistPracticeMutation({
 *   variables: {
 *      dentistId: // value for 'dentistId'
 *      practiceId: // value for 'practiceId'
 *   },
 * });
 */
export function useAddDentistPracticeMutation(baseOptions?: Apollo.MutationHookOptions<AddDentistPracticeMutation, AddDentistPracticeMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddDentistPracticeMutation, AddDentistPracticeMutationVariables>(AddDentistPracticeDocument, options);
      }
export type AddDentistPracticeMutationHookResult = ReturnType<typeof useAddDentistPracticeMutation>;
export type AddDentistPracticeMutationResult = Apollo.MutationResult<AddDentistPracticeMutation>;
export type AddDentistPracticeMutationOptions = Apollo.BaseMutationOptions<AddDentistPracticeMutation, AddDentistPracticeMutationVariables>;
export const AddDentistSettingsDocument = gql`
    mutation AddDentistSettings($data: DentistProfileInput!) {
  addDentistSettings(data: $data) {
    id
    name
    firstName
    lastName
    email
    postIndex
    city
    street
    phone
    website
    qualifications
    bio
    lat
    lng
  }
}
    `;
export type AddDentistSettingsMutationFn = Apollo.MutationFunction<AddDentistSettingsMutation, AddDentistSettingsMutationVariables>;
export type AddDentistSettingsComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<AddDentistSettingsMutation, AddDentistSettingsMutationVariables>, 'mutation'>;

    export const AddDentistSettingsComponent = (props: AddDentistSettingsComponentProps) => (
      <ApolloReactComponents.Mutation<AddDentistSettingsMutation, AddDentistSettingsMutationVariables> mutation={AddDentistSettingsDocument} {...props} />
    );
    

/**
 * __useAddDentistSettingsMutation__
 *
 * To run a mutation, you first call `useAddDentistSettingsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddDentistSettingsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addDentistSettingsMutation, { data, loading, error }] = useAddDentistSettingsMutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useAddDentistSettingsMutation(baseOptions?: Apollo.MutationHookOptions<AddDentistSettingsMutation, AddDentistSettingsMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddDentistSettingsMutation, AddDentistSettingsMutationVariables>(AddDentistSettingsDocument, options);
      }
export type AddDentistSettingsMutationHookResult = ReturnType<typeof useAddDentistSettingsMutation>;
export type AddDentistSettingsMutationResult = Apollo.MutationResult<AddDentistSettingsMutation>;
export type AddDentistSettingsMutationOptions = Apollo.BaseMutationOptions<AddDentistSettingsMutation, AddDentistSettingsMutationVariables>;
export const CreatePracticeDocument = gql`
    mutation CreatePractice($practice: String!, $dentistId: Int!) {
  createPractice(practice: $practice, dentistId: $dentistId)
}
    `;
export type CreatePracticeMutationFn = Apollo.MutationFunction<CreatePracticeMutation, CreatePracticeMutationVariables>;
export type CreatePracticeComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<CreatePracticeMutation, CreatePracticeMutationVariables>, 'mutation'>;

    export const CreatePracticeComponent = (props: CreatePracticeComponentProps) => (
      <ApolloReactComponents.Mutation<CreatePracticeMutation, CreatePracticeMutationVariables> mutation={CreatePracticeDocument} {...props} />
    );
    

/**
 * __useCreatePracticeMutation__
 *
 * To run a mutation, you first call `useCreatePracticeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreatePracticeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createPracticeMutation, { data, loading, error }] = useCreatePracticeMutation({
 *   variables: {
 *      practice: // value for 'practice'
 *      dentistId: // value for 'dentistId'
 *   },
 * });
 */
export function useCreatePracticeMutation(baseOptions?: Apollo.MutationHookOptions<CreatePracticeMutation, CreatePracticeMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreatePracticeMutation, CreatePracticeMutationVariables>(CreatePracticeDocument, options);
      }
export type CreatePracticeMutationHookResult = ReturnType<typeof useCreatePracticeMutation>;
export type CreatePracticeMutationResult = Apollo.MutationResult<CreatePracticeMutation>;
export type CreatePracticeMutationOptions = Apollo.BaseMutationOptions<CreatePracticeMutation, CreatePracticeMutationVariables>;
export const CreateServiceDocument = gql`
    mutation CreateService($service: String!, $dentistId: Int!) {
  createService(service: $service, dentistId: $dentistId)
}
    `;
export type CreateServiceMutationFn = Apollo.MutationFunction<CreateServiceMutation, CreateServiceMutationVariables>;
export type CreateServiceComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<CreateServiceMutation, CreateServiceMutationVariables>, 'mutation'>;

    export const CreateServiceComponent = (props: CreateServiceComponentProps) => (
      <ApolloReactComponents.Mutation<CreateServiceMutation, CreateServiceMutationVariables> mutation={CreateServiceDocument} {...props} />
    );
    

/**
 * __useCreateServiceMutation__
 *
 * To run a mutation, you first call `useCreateServiceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateServiceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createServiceMutation, { data, loading, error }] = useCreateServiceMutation({
 *   variables: {
 *      service: // value for 'service'
 *      dentistId: // value for 'dentistId'
 *   },
 * });
 */
export function useCreateServiceMutation(baseOptions?: Apollo.MutationHookOptions<CreateServiceMutation, CreateServiceMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateServiceMutation, CreateServiceMutationVariables>(CreateServiceDocument, options);
      }
export type CreateServiceMutationHookResult = ReturnType<typeof useCreateServiceMutation>;
export type CreateServiceMutationResult = Apollo.MutationResult<CreateServiceMutation>;
export type CreateServiceMutationOptions = Apollo.BaseMutationOptions<CreateServiceMutation, CreateServiceMutationVariables>;
export const ChangePasswordDocument = gql`
    mutation ChangePassword($data: ChangePasswordInput!) {
  changePassword(data: $data) {
    id
    firstName
    lastName
    email
    name
  }
}
    `;
export type ChangePasswordMutationFn = Apollo.MutationFunction<ChangePasswordMutation, ChangePasswordMutationVariables>;
export type ChangePasswordComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<ChangePasswordMutation, ChangePasswordMutationVariables>, 'mutation'>;

    export const ChangePasswordComponent = (props: ChangePasswordComponentProps) => (
      <ApolloReactComponents.Mutation<ChangePasswordMutation, ChangePasswordMutationVariables> mutation={ChangePasswordDocument} {...props} />
    );
    

/**
 * __useChangePasswordMutation__
 *
 * To run a mutation, you first call `useChangePasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangePasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changePasswordMutation, { data, loading, error }] = useChangePasswordMutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useChangePasswordMutation(baseOptions?: Apollo.MutationHookOptions<ChangePasswordMutation, ChangePasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangePasswordMutation, ChangePasswordMutationVariables>(ChangePasswordDocument, options);
      }
export type ChangePasswordMutationHookResult = ReturnType<typeof useChangePasswordMutation>;
export type ChangePasswordMutationResult = Apollo.MutationResult<ChangePasswordMutation>;
export type ChangePasswordMutationOptions = Apollo.BaseMutationOptions<ChangePasswordMutation, ChangePasswordMutationVariables>;
export const ConfirmDentistDocument = gql`
    mutation ConfirmDentist($token: String!) {
  confirmDentist(token: $token)
}
    `;
export type ConfirmDentistMutationFn = Apollo.MutationFunction<ConfirmDentistMutation, ConfirmDentistMutationVariables>;
export type ConfirmDentistComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<ConfirmDentistMutation, ConfirmDentistMutationVariables>, 'mutation'>;

    export const ConfirmDentistComponent = (props: ConfirmDentistComponentProps) => (
      <ApolloReactComponents.Mutation<ConfirmDentistMutation, ConfirmDentistMutationVariables> mutation={ConfirmDentistDocument} {...props} />
    );
    

/**
 * __useConfirmDentistMutation__
 *
 * To run a mutation, you first call `useConfirmDentistMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useConfirmDentistMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [confirmDentistMutation, { data, loading, error }] = useConfirmDentistMutation({
 *   variables: {
 *      token: // value for 'token'
 *   },
 * });
 */
export function useConfirmDentistMutation(baseOptions?: Apollo.MutationHookOptions<ConfirmDentistMutation, ConfirmDentistMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ConfirmDentistMutation, ConfirmDentistMutationVariables>(ConfirmDentistDocument, options);
      }
export type ConfirmDentistMutationHookResult = ReturnType<typeof useConfirmDentistMutation>;
export type ConfirmDentistMutationResult = Apollo.MutationResult<ConfirmDentistMutation>;
export type ConfirmDentistMutationOptions = Apollo.BaseMutationOptions<ConfirmDentistMutation, ConfirmDentistMutationVariables>;
export const DeletePracticeDocument = gql`
    mutation DeletePractice($practiceId: Int!) {
  deletePractice(practiceId: $practiceId)
}
    `;
export type DeletePracticeMutationFn = Apollo.MutationFunction<DeletePracticeMutation, DeletePracticeMutationVariables>;
export type DeletePracticeComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<DeletePracticeMutation, DeletePracticeMutationVariables>, 'mutation'>;

    export const DeletePracticeComponent = (props: DeletePracticeComponentProps) => (
      <ApolloReactComponents.Mutation<DeletePracticeMutation, DeletePracticeMutationVariables> mutation={DeletePracticeDocument} {...props} />
    );
    

/**
 * __useDeletePracticeMutation__
 *
 * To run a mutation, you first call `useDeletePracticeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeletePracticeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deletePracticeMutation, { data, loading, error }] = useDeletePracticeMutation({
 *   variables: {
 *      practiceId: // value for 'practiceId'
 *   },
 * });
 */
export function useDeletePracticeMutation(baseOptions?: Apollo.MutationHookOptions<DeletePracticeMutation, DeletePracticeMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeletePracticeMutation, DeletePracticeMutationVariables>(DeletePracticeDocument, options);
      }
export type DeletePracticeMutationHookResult = ReturnType<typeof useDeletePracticeMutation>;
export type DeletePracticeMutationResult = Apollo.MutationResult<DeletePracticeMutation>;
export type DeletePracticeMutationOptions = Apollo.BaseMutationOptions<DeletePracticeMutation, DeletePracticeMutationVariables>;
export const DeleteServiceDocument = gql`
    mutation DeleteService($serviceId: Int!) {
  deleteService(serviceId: $serviceId)
}
    `;
export type DeleteServiceMutationFn = Apollo.MutationFunction<DeleteServiceMutation, DeleteServiceMutationVariables>;
export type DeleteServiceComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<DeleteServiceMutation, DeleteServiceMutationVariables>, 'mutation'>;

    export const DeleteServiceComponent = (props: DeleteServiceComponentProps) => (
      <ApolloReactComponents.Mutation<DeleteServiceMutation, DeleteServiceMutationVariables> mutation={DeleteServiceDocument} {...props} />
    );
    

/**
 * __useDeleteServiceMutation__
 *
 * To run a mutation, you first call `useDeleteServiceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteServiceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteServiceMutation, { data, loading, error }] = useDeleteServiceMutation({
 *   variables: {
 *      serviceId: // value for 'serviceId'
 *   },
 * });
 */
export function useDeleteServiceMutation(baseOptions?: Apollo.MutationHookOptions<DeleteServiceMutation, DeleteServiceMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteServiceMutation, DeleteServiceMutationVariables>(DeleteServiceDocument, options);
      }
export type DeleteServiceMutationHookResult = ReturnType<typeof useDeleteServiceMutation>;
export type DeleteServiceMutationResult = Apollo.MutationResult<DeleteServiceMutation>;
export type DeleteServiceMutationOptions = Apollo.BaseMutationOptions<DeleteServiceMutation, DeleteServiceMutationVariables>;
export const ForgotPasswordDocument = gql`
    mutation ForgotPassword($email: String!) {
  forgotPassword(email: $email)
}
    `;
export type ForgotPasswordMutationFn = Apollo.MutationFunction<ForgotPasswordMutation, ForgotPasswordMutationVariables>;
export type ForgotPasswordComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<ForgotPasswordMutation, ForgotPasswordMutationVariables>, 'mutation'>;

    export const ForgotPasswordComponent = (props: ForgotPasswordComponentProps) => (
      <ApolloReactComponents.Mutation<ForgotPasswordMutation, ForgotPasswordMutationVariables> mutation={ForgotPasswordDocument} {...props} />
    );
    

/**
 * __useForgotPasswordMutation__
 *
 * To run a mutation, you first call `useForgotPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useForgotPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [forgotPasswordMutation, { data, loading, error }] = useForgotPasswordMutation({
 *   variables: {
 *      email: // value for 'email'
 *   },
 * });
 */
export function useForgotPasswordMutation(baseOptions?: Apollo.MutationHookOptions<ForgotPasswordMutation, ForgotPasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ForgotPasswordMutation, ForgotPasswordMutationVariables>(ForgotPasswordDocument, options);
      }
export type ForgotPasswordMutationHookResult = ReturnType<typeof useForgotPasswordMutation>;
export type ForgotPasswordMutationResult = Apollo.MutationResult<ForgotPasswordMutation>;
export type ForgotPasswordMutationOptions = Apollo.BaseMutationOptions<ForgotPasswordMutation, ForgotPasswordMutationVariables>;
export const LoginDocument = gql`
    mutation Login($email: String!, $password: String!) {
  login(email: $email, password: $password) {
    id
    firstName
    lastName
    email
    name
  }
}
    `;
export type LoginMutationFn = Apollo.MutationFunction<LoginMutation, LoginMutationVariables>;
export type LoginComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<LoginMutation, LoginMutationVariables>, 'mutation'>;

    export const LoginComponent = (props: LoginComponentProps) => (
      <ApolloReactComponents.Mutation<LoginMutation, LoginMutationVariables> mutation={LoginDocument} {...props} />
    );
    

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(baseOptions?: Apollo.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, options);
      }
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = Apollo.MutationResult<LoginMutation>;
export type LoginMutationOptions = Apollo.BaseMutationOptions<LoginMutation, LoginMutationVariables>;
export const LogoutDocument = gql`
    mutation Logout {
  logout
}
    `;
export type LogoutMutationFn = Apollo.MutationFunction<LogoutMutation, LogoutMutationVariables>;
export type LogoutComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<LogoutMutation, LogoutMutationVariables>, 'mutation'>;

    export const LogoutComponent = (props: LogoutComponentProps) => (
      <ApolloReactComponents.Mutation<LogoutMutation, LogoutMutationVariables> mutation={LogoutDocument} {...props} />
    );
    

/**
 * __useLogoutMutation__
 *
 * To run a mutation, you first call `useLogoutMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLogoutMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [logoutMutation, { data, loading, error }] = useLogoutMutation({
 *   variables: {
 *   },
 * });
 */
export function useLogoutMutation(baseOptions?: Apollo.MutationHookOptions<LogoutMutation, LogoutMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LogoutMutation, LogoutMutationVariables>(LogoutDocument, options);
      }
export type LogoutMutationHookResult = ReturnType<typeof useLogoutMutation>;
export type LogoutMutationResult = Apollo.MutationResult<LogoutMutation>;
export type LogoutMutationOptions = Apollo.BaseMutationOptions<LogoutMutation, LogoutMutationVariables>;
export const RegisterDocument = gql`
    mutation Register($data: RegisterInput!) {
  register(data: $data) {
    id
    firstName
    lastName
    email
    name
  }
}
    `;
export type RegisterMutationFn = Apollo.MutationFunction<RegisterMutation, RegisterMutationVariables>;
export type RegisterComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<RegisterMutation, RegisterMutationVariables>, 'mutation'>;

    export const RegisterComponent = (props: RegisterComponentProps) => (
      <ApolloReactComponents.Mutation<RegisterMutation, RegisterMutationVariables> mutation={RegisterDocument} {...props} />
    );
    

/**
 * __useRegisterMutation__
 *
 * To run a mutation, you first call `useRegisterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerMutation, { data, loading, error }] = useRegisterMutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useRegisterMutation(baseOptions?: Apollo.MutationHookOptions<RegisterMutation, RegisterMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RegisterMutation, RegisterMutationVariables>(RegisterDocument, options);
      }
export type RegisterMutationHookResult = ReturnType<typeof useRegisterMutation>;
export type RegisterMutationResult = Apollo.MutationResult<RegisterMutation>;
export type RegisterMutationOptions = Apollo.BaseMutationOptions<RegisterMutation, RegisterMutationVariables>;
export const AddProfilePictureDocument = gql`
    mutation AddProfilePicture($picture: Upload!) {
  addProfilePicture(picture: $picture)
}
    `;
export type AddProfilePictureMutationFn = Apollo.MutationFunction<AddProfilePictureMutation, AddProfilePictureMutationVariables>;
export type AddProfilePictureComponentProps = Omit<ApolloReactComponents.MutationComponentOptions<AddProfilePictureMutation, AddProfilePictureMutationVariables>, 'mutation'>;

    export const AddProfilePictureComponent = (props: AddProfilePictureComponentProps) => (
      <ApolloReactComponents.Mutation<AddProfilePictureMutation, AddProfilePictureMutationVariables> mutation={AddProfilePictureDocument} {...props} />
    );
    

/**
 * __useAddProfilePictureMutation__
 *
 * To run a mutation, you first call `useAddProfilePictureMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddProfilePictureMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addProfilePictureMutation, { data, loading, error }] = useAddProfilePictureMutation({
 *   variables: {
 *      picture: // value for 'picture'
 *   },
 * });
 */
export function useAddProfilePictureMutation(baseOptions?: Apollo.MutationHookOptions<AddProfilePictureMutation, AddProfilePictureMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AddProfilePictureMutation, AddProfilePictureMutationVariables>(AddProfilePictureDocument, options);
      }
export type AddProfilePictureMutationHookResult = ReturnType<typeof useAddProfilePictureMutation>;
export type AddProfilePictureMutationResult = Apollo.MutationResult<AddProfilePictureMutation>;
export type AddProfilePictureMutationOptions = Apollo.BaseMutationOptions<AddProfilePictureMutation, AddProfilePictureMutationVariables>;
export const DentistsDocument = gql`
    query Dentists {
  dentists {
    id
    postIndex
    street
    bio
    city
    phone
    email
    firstName
    lastName
    name
    website
    qualifications
    address
    lat
    lng
    practices {
      practice {
        id
        name
      }
    }
    services {
      service {
        id
        name
      }
    }
  }
}
    `;
export type DentistsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<DentistsQuery, DentistsQueryVariables>, 'query'>;

    export const DentistsComponent = (props: DentistsComponentProps) => (
      <ApolloReactComponents.Query<DentistsQuery, DentistsQueryVariables> query={DentistsDocument} {...props} />
    );
    

/**
 * __useDentistsQuery__
 *
 * To run a query within a React component, call `useDentistsQuery` and pass it any options that fit your needs.
 * When your component renders, `useDentistsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useDentistsQuery({
 *   variables: {
 *   },
 * });
 */
export function useDentistsQuery(baseOptions?: Apollo.QueryHookOptions<DentistsQuery, DentistsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<DentistsQuery, DentistsQueryVariables>(DentistsDocument, options);
      }
export function useDentistsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<DentistsQuery, DentistsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<DentistsQuery, DentistsQueryVariables>(DentistsDocument, options);
        }
export type DentistsQueryHookResult = ReturnType<typeof useDentistsQuery>;
export type DentistsLazyQueryHookResult = ReturnType<typeof useDentistsLazyQuery>;
export type DentistsQueryResult = Apollo.QueryResult<DentistsQuery, DentistsQueryVariables>;
export const DentistDocument = gql`
    query Dentist($id: Int!) {
  dentist(id: $id) {
    id
    postIndex
    street
    bio
    city
    phone
    email
    firstName
    lastName
    name
    website
    qualifications
    address
    lat
    lng
    practices {
      practice {
        id
        name
      }
    }
    services {
      service {
        id
        name
      }
    }
  }
}
    `;
export type DentistComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<DentistQuery, DentistQueryVariables>, 'query'> & ({ variables: DentistQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const DentistComponent = (props: DentistComponentProps) => (
      <ApolloReactComponents.Query<DentistQuery, DentistQueryVariables> query={DentistDocument} {...props} />
    );
    

/**
 * __useDentistQuery__
 *
 * To run a query within a React component, call `useDentistQuery` and pass it any options that fit your needs.
 * When your component renders, `useDentistQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useDentistQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDentistQuery(baseOptions: Apollo.QueryHookOptions<DentistQuery, DentistQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<DentistQuery, DentistQueryVariables>(DentistDocument, options);
      }
export function useDentistLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<DentistQuery, DentistQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<DentistQuery, DentistQueryVariables>(DentistDocument, options);
        }
export type DentistQueryHookResult = ReturnType<typeof useDentistQuery>;
export type DentistLazyQueryHookResult = ReturnType<typeof useDentistLazyQuery>;
export type DentistQueryResult = Apollo.QueryResult<DentistQuery, DentistQueryVariables>;
export const CoordinatesDocument = gql`
    query Coordinates {
  coordinates {
    id
    lat
    lng
    dentistConnection {
      dentist {
        id
        name
      }
    }
  }
}
    `;
export type CoordinatesComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<CoordinatesQuery, CoordinatesQueryVariables>, 'query'>;

    export const CoordinatesComponent = (props: CoordinatesComponentProps) => (
      <ApolloReactComponents.Query<CoordinatesQuery, CoordinatesQueryVariables> query={CoordinatesDocument} {...props} />
    );
    

/**
 * __useCoordinatesQuery__
 *
 * To run a query within a React component, call `useCoordinatesQuery` and pass it any options that fit your needs.
 * When your component renders, `useCoordinatesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCoordinatesQuery({
 *   variables: {
 *   },
 * });
 */
export function useCoordinatesQuery(baseOptions?: Apollo.QueryHookOptions<CoordinatesQuery, CoordinatesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CoordinatesQuery, CoordinatesQueryVariables>(CoordinatesDocument, options);
      }
export function useCoordinatesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CoordinatesQuery, CoordinatesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CoordinatesQuery, CoordinatesQueryVariables>(CoordinatesDocument, options);
        }
export type CoordinatesQueryHookResult = ReturnType<typeof useCoordinatesQuery>;
export type CoordinatesLazyQueryHookResult = ReturnType<typeof useCoordinatesLazyQuery>;
export type CoordinatesQueryResult = Apollo.QueryResult<CoordinatesQuery, CoordinatesQueryVariables>;
export const MeDocument = gql`
    query Me {
  me {
    id
    postIndex
    street
    bio
    city
    phone
    email
    firstName
    lastName
    name
    website
    qualifications
    address
    lat
    lng
    practices {
      practice {
        id
        name
      }
    }
    services {
      service {
        id
        name
      }
    }
  }
}
    `;
export type MeComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<MeQuery, MeQueryVariables>, 'query'>;

    export const MeComponent = (props: MeComponentProps) => (
      <ApolloReactComponents.Query<MeQuery, MeQueryVariables> query={MeDocument} {...props} />
    );
    

/**
 * __useMeQuery__
 *
 * To run a query within a React component, call `useMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMeQuery({
 *   variables: {
 *   },
 * });
 */
export function useMeQuery(baseOptions?: Apollo.QueryHookOptions<MeQuery, MeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MeQuery, MeQueryVariables>(MeDocument, options);
      }
export function useMeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MeQuery, MeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MeQuery, MeQueryVariables>(MeDocument, options);
        }
export type MeQueryHookResult = ReturnType<typeof useMeQuery>;
export type MeLazyQueryHookResult = ReturnType<typeof useMeLazyQuery>;
export type MeQueryResult = Apollo.QueryResult<MeQuery, MeQueryVariables>;
export const PracticesDocument = gql`
    query Practices {
  practices {
    id
    name
    dentists {
      dentist {
        id
        postIndex
        street
        bio
        city
        phone
        email
        firstName
        lastName
        name
        website
        qualifications
        lat
        lng
      }
    }
  }
}
    `;
export type PracticesComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<PracticesQuery, PracticesQueryVariables>, 'query'>;

    export const PracticesComponent = (props: PracticesComponentProps) => (
      <ApolloReactComponents.Query<PracticesQuery, PracticesQueryVariables> query={PracticesDocument} {...props} />
    );
    

/**
 * __usePracticesQuery__
 *
 * To run a query within a React component, call `usePracticesQuery` and pass it any options that fit your needs.
 * When your component renders, `usePracticesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePracticesQuery({
 *   variables: {
 *   },
 * });
 */
export function usePracticesQuery(baseOptions?: Apollo.QueryHookOptions<PracticesQuery, PracticesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<PracticesQuery, PracticesQueryVariables>(PracticesDocument, options);
      }
export function usePracticesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<PracticesQuery, PracticesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<PracticesQuery, PracticesQueryVariables>(PracticesDocument, options);
        }
export type PracticesQueryHookResult = ReturnType<typeof usePracticesQuery>;
export type PracticesLazyQueryHookResult = ReturnType<typeof usePracticesLazyQuery>;
export type PracticesQueryResult = Apollo.QueryResult<PracticesQuery, PracticesQueryVariables>;
export const PracticeDocument = gql`
    query Practice($practice: String!) {
  practice(practice: $practice) {
    id
    name
    dentists {
      dentist {
        id
        postIndex
        street
        bio
        city
        phone
        email
        firstName
        lastName
        name
        website
        qualifications
        lat
        lng
      }
    }
  }
}
    `;
export type PracticeComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<PracticeQuery, PracticeQueryVariables>, 'query'> & ({ variables: PracticeQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const PracticeComponent = (props: PracticeComponentProps) => (
      <ApolloReactComponents.Query<PracticeQuery, PracticeQueryVariables> query={PracticeDocument} {...props} />
    );
    

/**
 * __usePracticeQuery__
 *
 * To run a query within a React component, call `usePracticeQuery` and pass it any options that fit your needs.
 * When your component renders, `usePracticeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePracticeQuery({
 *   variables: {
 *      practice: // value for 'practice'
 *   },
 * });
 */
export function usePracticeQuery(baseOptions: Apollo.QueryHookOptions<PracticeQuery, PracticeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<PracticeQuery, PracticeQueryVariables>(PracticeDocument, options);
      }
export function usePracticeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<PracticeQuery, PracticeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<PracticeQuery, PracticeQueryVariables>(PracticeDocument, options);
        }
export type PracticeQueryHookResult = ReturnType<typeof usePracticeQuery>;
export type PracticeLazyQueryHookResult = ReturnType<typeof usePracticeLazyQuery>;
export type PracticeQueryResult = Apollo.QueryResult<PracticeQuery, PracticeQueryVariables>;
export const QuerySearchDentistsDocument = gql`
    query QuerySearchDentists($lat: Float!, $lng: Float!) {
  querySearchDentist(lat: $lat, lng: $lng) {
    id
    postIndex
    street
    bio
    city
    phone
    email
    firstName
    lastName
    name
    website
    qualifications
    address
    lat
    lng
  }
}
    `;
export type QuerySearchDentistsComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<QuerySearchDentistsQuery, QuerySearchDentistsQueryVariables>, 'query'> & ({ variables: QuerySearchDentistsQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const QuerySearchDentistsComponent = (props: QuerySearchDentistsComponentProps) => (
      <ApolloReactComponents.Query<QuerySearchDentistsQuery, QuerySearchDentistsQueryVariables> query={QuerySearchDentistsDocument} {...props} />
    );
    

/**
 * __useQuerySearchDentistsQuery__
 *
 * To run a query within a React component, call `useQuerySearchDentistsQuery` and pass it any options that fit your needs.
 * When your component renders, `useQuerySearchDentistsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useQuerySearchDentistsQuery({
 *   variables: {
 *      lat: // value for 'lat'
 *      lng: // value for 'lng'
 *   },
 * });
 */
export function useQuerySearchDentistsQuery(baseOptions: Apollo.QueryHookOptions<QuerySearchDentistsQuery, QuerySearchDentistsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<QuerySearchDentistsQuery, QuerySearchDentistsQueryVariables>(QuerySearchDentistsDocument, options);
      }
export function useQuerySearchDentistsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<QuerySearchDentistsQuery, QuerySearchDentistsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<QuerySearchDentistsQuery, QuerySearchDentistsQueryVariables>(QuerySearchDentistsDocument, options);
        }
export type QuerySearchDentistsQueryHookResult = ReturnType<typeof useQuerySearchDentistsQuery>;
export type QuerySearchDentistsLazyQueryHookResult = ReturnType<typeof useQuerySearchDentistsLazyQuery>;
export type QuerySearchDentistsQueryResult = Apollo.QueryResult<QuerySearchDentistsQuery, QuerySearchDentistsQueryVariables>;
export const ServicesDocument = gql`
    query Services {
  services {
    id
    name
    dentists {
      dentist {
        id
        postIndex
        street
        bio
        city
        phone
        email
        firstName
        lastName
        name
        website
        qualifications
        lat
        lng
      }
    }
  }
}
    `;
export type ServicesComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<ServicesQuery, ServicesQueryVariables>, 'query'>;

    export const ServicesComponent = (props: ServicesComponentProps) => (
      <ApolloReactComponents.Query<ServicesQuery, ServicesQueryVariables> query={ServicesDocument} {...props} />
    );
    

/**
 * __useServicesQuery__
 *
 * To run a query within a React component, call `useServicesQuery` and pass it any options that fit your needs.
 * When your component renders, `useServicesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useServicesQuery({
 *   variables: {
 *   },
 * });
 */
export function useServicesQuery(baseOptions?: Apollo.QueryHookOptions<ServicesQuery, ServicesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ServicesQuery, ServicesQueryVariables>(ServicesDocument, options);
      }
export function useServicesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ServicesQuery, ServicesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ServicesQuery, ServicesQueryVariables>(ServicesDocument, options);
        }
export type ServicesQueryHookResult = ReturnType<typeof useServicesQuery>;
export type ServicesLazyQueryHookResult = ReturnType<typeof useServicesLazyQuery>;
export type ServicesQueryResult = Apollo.QueryResult<ServicesQuery, ServicesQueryVariables>;
export const ServiceDocument = gql`
    query Service($service: String!) {
  service(service: $service) {
    id
    name
    dentists {
      dentist {
        id
        postIndex
        street
        bio
        city
        phone
        email
        firstName
        lastName
        name
        website
        qualifications
        lat
        lng
      }
    }
  }
}
    `;
export type ServiceComponentProps = Omit<ApolloReactComponents.QueryComponentOptions<ServiceQuery, ServiceQueryVariables>, 'query'> & ({ variables: ServiceQueryVariables; skip?: boolean; } | { skip: boolean; });

    export const ServiceComponent = (props: ServiceComponentProps) => (
      <ApolloReactComponents.Query<ServiceQuery, ServiceQueryVariables> query={ServiceDocument} {...props} />
    );
    

/**
 * __useServiceQuery__
 *
 * To run a query within a React component, call `useServiceQuery` and pass it any options that fit your needs.
 * When your component renders, `useServiceQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useServiceQuery({
 *   variables: {
 *      service: // value for 'service'
 *   },
 * });
 */
export function useServiceQuery(baseOptions: Apollo.QueryHookOptions<ServiceQuery, ServiceQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ServiceQuery, ServiceQueryVariables>(ServiceDocument, options);
      }
export function useServiceLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ServiceQuery, ServiceQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ServiceQuery, ServiceQueryVariables>(ServiceDocument, options);
        }
export type ServiceQueryHookResult = ReturnType<typeof useServiceQuery>;
export type ServiceLazyQueryHookResult = ReturnType<typeof useServiceLazyQuery>;
export type ServiceQueryResult = Apollo.QueryResult<ServiceQuery, ServiceQueryVariables>;
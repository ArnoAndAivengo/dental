import styled from "styled-components";

export const AddWatermarkBlock = styled("div")`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  align-items: center;
`;



export const BigSquare = styled("div")`
  width: 204px;
  height: 49px;
  background: #E1E1E1 0 0 no-repeat padding-box;
  border: 1px solid #707070;
  text-align: left;
  font: normal normal normal 13px/17px Segoe UI;
  letter-spacing: 0px;
  color: #A2A2A2;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 22px;
`;

export const BlockEmptyImage = styled("div")`
  display: block;
  background-color: #0d9da6;
  width: 150px;
  height: 150px;
  margin: 15px;
  border: 1px solid #0d9da6;
`;
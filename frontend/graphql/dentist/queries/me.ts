import {gql} from "apollo-boost";
// @ts-ignore
export const meQuery = gql`
    query Me {
        me {
            id
            postIndex
            street
            bio
            city
            phone
            email
            firstName
            lastName
            name
            website
            qualifications
            address
            lat
            lng
            practices {
                practice {
                    id
                    name
                }
            }
            services {
                service {
                    id
                    name
                }
            }
        }
    }
`;

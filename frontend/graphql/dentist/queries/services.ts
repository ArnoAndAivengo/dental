import {gql} from "apollo-boost";
// @ts-ignore
export const servicesQuery = gql`
    query Services {
        services{
            id
            name
            dentists {
                dentist {
                    id
                    postIndex
                    street
                    bio
                    city
                    phone
                    email
                    firstName
                    lastName
                    name
                    website
                    qualifications
                    lat
                    lng
                }
            }
        }
    }
`;
// @ts-ignore
export const serviceQuery = gql`
    query Service($service: String!) {
        service(service: $service){
            id
            name
            dentists {
                dentist {
                    id
                    postIndex
                    street
                    bio
                    city
                    phone
                    email
                    firstName
                    lastName
                    name
                    website
                    qualifications
                    lat
                    lng
                }
            }
        }
    }
`;

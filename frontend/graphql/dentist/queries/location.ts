import {gql} from "apollo-boost";
// @ts-ignore
export const coordinatesQuery = gql`
    query Coordinates {
        coordinates {
            id
            lat
            lng
            dentistConnection {
                dentist {
                    id
                    name
                }
            }
        }
    }
`;
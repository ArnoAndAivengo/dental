import {gql} from "apollo-boost";
// @ts-ignore
export const dentistsQuery = gql`
    query Dentists {
        dentists {
            id
            postIndex
            street
            bio
            city
            phone
            email
            firstName
            lastName
            name
            website
            qualifications
            address
            lat
            lng
            practices {
                practice {
                    id
                    name
                }
            }
            services {
                service {
                    id
                    name
                }
            }
        }
    }
`;
// @ts-ignore
export const dentistQuery = gql`
    query Dentist($id: Int!) {
        dentist(id: $id){
            id
            postIndex
            street
            bio
            city
            phone
            email
            firstName
            lastName
            name
            website
            qualifications
            address
            lat
            lng
            practices {
                practice {
                    id
                    name
                }
            }
            services {
                service {
                    id
                    name
                }
            }
        }
    }
`;

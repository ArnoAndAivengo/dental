import {gql} from "apollo-boost";
// @ts-ignore
export const practicesQuery = gql`
    query Practices {
        practices{
            id
            name
            dentists {
                dentist {
                    id
                    postIndex
                    street
                    bio
                    city
                    phone
                    email
                    firstName
                    lastName
                    name
                    website
                    qualifications
                    lat
                    lng
                }
            }
        }
    }
`;
// @ts-ignore
export const practiceQuery = gql`
    query Practice($practice: String!) {
        practice(practice: $practice){
            id
            name
            dentists {
                dentist {
                    id
                    postIndex
                    street
                    bio
                    city
                    phone
                    email
                    firstName
                    lastName
                    name
                    website
                    qualifications
                    lat
                    lng
                }
            }
        }
    }
`;

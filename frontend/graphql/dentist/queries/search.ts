import {gql} from "apollo-boost";
// @ts-ignore
export const vf = gql`
    query QuerySearchDentists($lat: Float!, $lng: Float!) {
        querySearchDentist(lat: $lat, lng: $lng) {
            id
            postIndex
            street
            bio
            city
            phone
            email
            firstName
            lastName
            name
            website
            qualifications
            address
            lat
            lng
        }
    }
`;

import {gql} from "apollo-boost";

export const addDentistPracticeMutation = gql`
    mutation AddDentistPractice($dentistId: Int!, $practiceId: Int!) {
        addDentistPractice(dentistId: $dentistId, practiceId: $practiceId)
    }
`;

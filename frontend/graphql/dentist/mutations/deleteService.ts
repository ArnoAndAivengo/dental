import { gql } from "apollo-boost";
// @ts-ignore
export const deleteServiceMutation = gql`
    mutation DeleteService($serviceId: Int!) {
        deleteService(serviceId: $serviceId)
    }
`;

import { gql } from "apollo-boost";
// @ts-ignore
export const addProfilePicture = gql`
  mutation AddProfilePicture($picture: Upload!) {
      addProfilePicture(picture: $picture)
  }
`
import { gql } from "apollo-boost";
// @ts-ignore
export const forgotPasswordMutation = gql`
  mutation ForgotPassword($email: String!) {
    forgotPassword(email: $email)
  }
`;

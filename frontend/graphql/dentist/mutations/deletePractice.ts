import { gql } from "apollo-boost";
// @ts-ignore
export const deletePracticeMutation = gql`
    mutation DeletePractice($practiceId: Int!) {
        deletePractice(practiceId: $practiceId)
    }
`;

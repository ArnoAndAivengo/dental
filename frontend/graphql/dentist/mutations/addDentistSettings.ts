import {gql} from "apollo-boost";

export const addDentistSettingsMutation = gql`
    mutation AddDentistSettings($data: DentistProfileInput!
    ) {
        addDentistSettings(data: $data
        ) {
            id
            name
            firstName
            lastName
            email
            postIndex
            city
            street
            phone
            website
            qualifications
            bio
            lat
            lng
        }
    }
`;

import { gql } from "apollo-boost";
// @ts-ignore
export const logoutMutation = gql`
  mutation Logout {
    logout
  }
`;

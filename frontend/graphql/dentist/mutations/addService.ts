import {gql} from "apollo-boost";
// @ts-ignore
export const createServiceMutation = gql`
    mutation CreateService($service: String!, $dentistId: Int!) {
        createService(service: $service, dentistId: $dentistId)
    }
`;

import {gql} from "apollo-boost";

export const createPracticeMutation = gql`
    mutation CreatePractice($practice: String!, $dentistId: Int!) {
        createPractice(practice: $practice, dentistId: $dentistId)
    }
`;


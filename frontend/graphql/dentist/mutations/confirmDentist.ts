import { gql } from "apollo-boost";
// @ts-ignore
export const confirmDentistMutation = gql`
  mutation ConfirmDentist($token: String!) {
    confirmDentist(token: $token)
  }
`;

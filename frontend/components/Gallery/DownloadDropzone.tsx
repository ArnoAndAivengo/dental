import React, {useState} from 'react'
import {DropzoneDialog} from 'material-ui-dropzone'
import {useMeQuery} from "../../generated/apolloComponents";
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';

type Props = {
  getImages: Function
}

const DownloadDropzone: React.FunctionComponent<Props> = ({getImages}) => {
  const {data} = useMeQuery();
  // @ts-ignore
  const [open, setOpen] = useState(false);
  const [downloadMessage, setDownloadMessage] = useState('');
  const [statusSnackbar, setStatusSnackbar] = useState('');
  const [openSnackbar, setOpenSnackbar] = useState(false);

  const handleClose = () => {
    setOpen(false)
  }
  const handleCloseSnackbar = () => {
    setOpenSnackbar(false)
  }

  const handleSave = async (files: any) => {
    const URL = "http://localhost:4000/uploads-file/" + data?.me?.id
    const formdata = new FormData();
    formdata.append("file", files[0], files[0].name);

    const requestOptions: any = {
      method: 'POST',
      body: formdata,
      redirect: 'follow'
    };

    fetch(URL, requestOptions)
      .then(response => response.json())
      .then(result => {
        getImages()
        setDownloadMessage(result.message)
        setStatusSnackbar('success')
        setOpenSnackbar(true)
        setOpen(false)
      })
      .catch((_error: any) => {
        setDownloadMessage('File upload error')
        setStatusSnackbar('error')
        setOpenSnackbar(true)
      });
  }

  const handleOpen = () => {
    setOpen(true)
  }

  return (
    <>
      <button className="search-button" onClick={handleOpen}>
        Upload New Image
      </button>
      <DropzoneDialog
        open={open}
        onSave={handleSave}
        acceptedFiles={['image/jpeg', 'image/png', 'image/bmp']}
        showPreviews={true}
        maxFileSize={5000000}
        onClose={handleClose}
      />
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        open={openSnackbar}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
      >
        <Alert
          variant="filled"
          // @ts-ignore
          severity={statusSnackbar}
        >
          {downloadMessage}
        </Alert>
      </Snackbar>
      <style jsx>{`
        .search-button {
          width: 100%;
          cursor: pointer;
          background: #fff;
          height: 47px;
          border-radius: 30px;
          border: 1px solid #0d9da6;
          display: flex;
          flex-direction: row;
          justify-content: center;
          align-items: center;
          padding: 0 20px;
          color: #000;
        }

        .search-button:hover {
          background: #0d9da6;
          color: #fff;
        }
      `}</style>
    </>
  )
}

export default DownloadDropzone;
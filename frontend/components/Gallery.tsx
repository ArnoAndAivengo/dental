import React from "react";
//@ts-ignore
import Gallery from 'react-grid-gallery';
import styled from "styled-components";

const GalleryWrapper = styled("div")`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  background: #FFFFFF 0 0 no-repeat padding-box;
  padding: 15px;
  margin: 10px 0;
  border-radius: 10px;
`;

type Props = {
  setDeleteImage?: any
  getImages: any
  images: any
}

const GalleryComponent: React.FunctionComponent<Props> = ({setDeleteImage, images, getImages}) => {

  React.useEffect(() => {
    getImages()
  }, []);

  const selectImage = (index: number) => {
    images[index].isSelected = !images[index].isSelected;
    setDeleteImage(images[index].name)
  }
  return (
    images.status !== 500 ? <GalleryWrapper style={{
      display: "block",
      minHeight: "1px",
      width: "100%",
      overflow: "auto"
    }}>
      <Gallery
        images={images}
        enableLightbox={true}
        enableImageSelection
        onSelectImage={selectImage}
      />
    </GalleryWrapper> : images ? <></> : <>...Loading</>
  )
}

export default GalleryComponent;
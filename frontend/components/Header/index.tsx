import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {MeComponent, useMeQuery} from "../../generated/apolloComponents";
import Link from "next/link";
import {Menu, MenuItem} from "@material-ui/core";
import PopupState, {bindMenu, bindTrigger} from 'material-ui-popup-state';

export default function Header() {

  const {data, loading, error} = useMeQuery();
  let currentDentist = ''

  if (error) {
    return (
      <>Error</>
    )
  }

  if (!data || loading || data.me) {
    currentDentist = `${data?.me?.id}`;
  }

  return (
    <AppBar position="static" style={{background: '#0d9da6'}}>
      <Toolbar style={{justifyContent: 'space-between', display: 'flex'}}>
        <div style={{alignItems: 'center', display: 'flex'}}>
          <IconButton edge="start" color="inherit" aria-label="menu">
            <PopupState variant="popover" popupId="demo-popup-menu">
              {(popupState) => (
                <React.Fragment>
                  <MenuIcon {...bindTrigger(popupState)}/>
                  <Menu {...bindMenu(popupState)}>
                    <MenuItem onClick={popupState.close}>
                      <Link href={"../../dentist/account/" + currentDentist}>Account</Link>
                    </MenuItem>
                    <MenuItem onClick={popupState.close}>
                      <Link href={"../../dentist/profile/" + currentDentist}><a>Profile</a></Link>
                    </MenuItem>
                    <MenuItem onClick={popupState.close}>
                      <Link href={"../../dentist/gallery/" + currentDentist}><a>Gallery</a></Link>
                    </MenuItem>
                    <MenuItem onClick={popupState.close}>
                      <Link href={"../../dentist/subscriptions/" + currentDentist}><a>Settings</a></Link>
                    </MenuItem>
                  </Menu>
                </React.Fragment>
              )}
            </PopupState>
          </IconButton>
          <Typography variant="h6">
            Dentist App
          </Typography>
        </div>
        <Link href={"../../../search"}>
          <Button color="inherit">Search</Button>
        </Link>
        <MeComponent>
          {({data, loading}) => {
            if (!data || loading || data.me) {
              return null;
            }
            return (
              <div>
                <Link href={"../../../login"}>
                  <Button color="inherit">Login</Button>
                </Link>
                <Link href={"../../../register"}>
                  <Button color="inherit">Register</Button>
                </Link>
              </div>
            );
          }}
        </MeComponent>
        <MeComponent>
          {({data, loading}) => {
            if (!data || loading || !data.me) {
              return null;
            }
            return (
                <div>
                  <Link href={"../../../logout"}>
                    <Button color="inherit">Logout</Button>
                  </Link>
                </div>
            );
          }}
        </MeComponent>
      </Toolbar>
    </AppBar>

  );
}
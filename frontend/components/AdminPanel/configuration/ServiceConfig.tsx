import React, {useState} from 'react';
import styled from "styled-components";
import {useServicesQuery, CreateServiceComponent, DeleteServiceComponent} from "../../../generated/apolloComponents";

const ServiceConfigBlock = styled("div")`{
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  border: 1px solid #0d9da6;
  padding: 20px;
`;

const ServiceListWrapper = styled("div")`{
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin-bottom: 30px;
`;

const Input = styled("input")`{
  padding: 10px;
  border-radius: 5px;
  border: 1px solid #e0e0e0;
  margin: 10px;
  width: 250px;

  &:active {
    & + span {
      width: 100%;
    }
  }
}
`;

const ConfirmButton = styled("button")`{
  width: 170px;
  cursor: pointer;
  background: #fff;
  height: 37px;
  border-radius: 30px;
  border: 1px solid #0d9da6;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 0 20px;
  color: #000;

  &:hover {
    background: #0d9da6;
    color: #fff;
  }
`;

const ServiceItem = styled("div")`{
  width: 250px;
  cursor: pointer;
  background: #fff;
  border-radius: 30px;
  border: 1px solid #0d9da6;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 5px;
  margin: 5px;
  color: #000;

  &:hover {
    background: #0d9da6;
    color: #fff;
  }
`;

const ServiceConfig = () => {

  const {data, loading, error, refetch} = useServicesQuery({
    variables: {},
  });

  if (error) {
    return (
      <>Error</>
    )
  }

  const [newService, setNewService] = useState('')

  return (
    !loading ?
      <ServiceConfigBlock>
        <div style={{textAlign: 'center'}}>
          Add the services the dentists can select from:
        </div>
        <Input type="text" name="service" onChange={event => setNewService(event.target.value)}/>
        <ServiceListWrapper>
          {
            data?.services.map((el, key) => {
              return (
                <DeleteServiceComponent key={key}>
                  {(mutate: (arg0: { variables: { serviceId: number; }; }) => any) => (
                    <ServiceItem key={el.id} onClick={async () => {
                      await mutate({
                        variables: {serviceId: Number(el.id)}
                      });
                      refetch();
                    }}>{el.name}</ServiceItem>
                  )}
                </DeleteServiceComponent>
              )
            })
          }
        </ServiceListWrapper>
        <CreateServiceComponent>
          {(mutate: (arg0: { variables: { service: string; dentistId: number }; }) => any) => (
            <ConfirmButton onClick={async () => {
              await mutate({variables: {service: newService, dentistId: 0}});
              refetch()
            }}>Confirm</ConfirmButton>
          )}
        </CreateServiceComponent>
      </ServiceConfigBlock>
      : <>...Loading</>
  );
}

export default ServiceConfig
import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import styled from "styled-components";
import SettingsIcon from '@material-ui/icons/Settings';
import {TablePagination} from "@material-ui/core";
import {useDentistsQuery} from "../../../generated/apolloComponents";

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
});

const ButtonProfileUser = styled("a")`{
  cursor: pointer;
  background: #fff;
  height: 47px;
  border-radius: 30px;
  border: 1px solid #0d9da6;
  padding: 10px 20px;
  color: #000;

  &:hover {
    background: #0d9da6;
    color: #fff;
  }
`;

const ButtonSettingsUser = styled("button")`{
  cursor: pointer;
  background: #fff;
  border-radius: 50%;
  border: 1px solid #0d9da6;
  padding: 8px 10px;
  color: #000;

  &:hover {
    background: #0d9da6;
    color: #fff;
  }
`;

interface Data {
  dentist: string;
  location: string;
  subscription: string;
  buttonProfile: Object;
  buttonSettings: Object;
}


export default function AdminTableUsers() {
  const {data, loading, error} = useDentistsQuery({
    variables: {},
  });

  if (error) {
    return (
      <>Error</>
    )
  }

  function createData(dentist: string, location: string, subscription: string, buttonProfile: Object, buttonSettings: Object): Data {
    return {dentist, location, subscription, buttonProfile, buttonSettings};
  }

  const profileUser = (id: string): any => {
    return (
      <ButtonProfileUser href={"../../dentist/account/" + id} target="_blank">View Profile</ButtonProfileUser>
    )
  }

  const settingsUser = (): any => {
    return (
      <ButtonSettingsUser><SettingsIcon/></ButtonSettingsUser>
    )
  }
  let rows: Data[] = []

  data?.dentists?.forEach(el => {
    rows.push(createData(el.name, el.address, "Subscription renewal: 12/12/2021", profileUser(el.id), settingsUser()))
  })

  interface Column {
    id: 'dentist' | 'location' | 'subscription' | 'buttonProfile' | 'buttonSettings';
    label: string;
    minWidth?: number;
    align?: 'right';
    format?: (value: number) => string;
  }

  const columns: Column[] = [
    {id: 'dentist', label: 'Dentist', minWidth: 170},
    {id: 'location', label: 'Location', minWidth: 100},
    {
      id: 'subscription',
      label: 'Subscription Status',
      minWidth: 170,

      format: (value: number) => value.toLocaleString('en-US'),
    },
    {
      id: 'buttonProfile',
      label: '',
      minWidth: 170,
    },
    {
      id: 'buttonSettings',
      label: '',
      minWidth: 170,
    },
  ];

  const classes = useStyles();

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (_event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    !loading ?
      <Paper>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{minWidth: column.minWidth}}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.dentist}>
                    {columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell key={column.id} align={column.align}>
                          {column.format && typeof value === 'number' ? column.format(value) : value}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    : <>...Loading</>
  );
}
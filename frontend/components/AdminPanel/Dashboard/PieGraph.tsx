import React from 'react';
import {Pie} from 'react-chartjs-2';

const data = {
  labels: [
    'Red',
    'Blue',
    'Yellow'
  ],
  datasets: [{
    label: 'My First Dataset',
    data: [300, 50, 100],
    backgroundColor: [
      'rgb(255, 99, 132)',
      'rgb(54, 162, 235)',
      'rgb(255, 205, 86)'
    ],
    hoverOffset: 4
  }]
};

const options = {
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
        },
      },
    ],
  },
};

type Props = {
  title?: string
}

const VerticalBar: React.FunctionComponent<Props> = ({title}) => {
  return (
    <>
      <div style={{height:'300px', width:'300px'}}>
        <Pie data={data} options={options} type='pie'/>
      </div>

      <div className='header'>
        <p className='title'>{title}</p>
        <div className='links'>
        </div>
      </div>
    </>
  )
};

export default VerticalBar;
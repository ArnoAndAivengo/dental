import React from "react";
import styled from "styled-components";
import Link from 'next/link'
// import {useMeQuery} from "../../generated/apolloComponents";

const Menu = styled("div")`{
  min-width: 250px;
  background: #ffffff 0 0 no-repeat padding-box;
  border-right: 1px solid #0d9da6;
  opacity: 1;
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  align-self: flex-start;

  a {
    text-align: center;
    font: normal normal normal 24px/32px Segoe UI;
    letter-spacing: 0px;
    color: #707070;
    opacity: 1;
    text-decoration: none;
    padding: 25px 0;
    border-bottom: 1px solid #0d9da6;
    width: 100%;

    &:hover {
      color: #FFFFFF;
      background: #037179 0 0 no-repeat padding-box;
    }
  }

  @media (max-width: 1024px) {
    display: none;
  }
`;

const MenuComponent = () => {

  // const {data, loading, error} = useMeQuery();
  // let currentDentist = ''
  //
  // if (error) {
  //   return (
  //     <>Error</>
  //   )
  // }
  //
  // if (!data || loading || data.me) {
  //   // @ts-ignore
  //   currentDentist = `${data?.me?.id}`;
  // }

  return (
    <Menu>
      <Link href={"../../admin/dashboard"}>Dashboard</Link>
      <Link href={"../../admin/users"}><a>Users</a></Link>
      <Link href={"../../admin/configuration"}><a>Configuration</a></Link>
    </Menu>
  );
}


export default MenuComponent;
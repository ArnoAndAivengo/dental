import React from "react";
import styled from "styled-components";

const ButtonFormWrapper = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 50px 0;
`;

const ButtonForm = styled("button")`
  background-color: #3f4b50;
  border: none;
  color: white;
  padding: 10px 20px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  border-radius: 5px;
  text-transform: uppercase;
`;

type Props = {
    title: string,
}

 const ButtonFormComponent: React.FunctionComponent<Props> = ({title}) => {
    return (
        <ButtonFormWrapper>
            <ButtonForm type="submit">
                {title}
            </ButtonForm>
        </ButtonFormWrapper>
    )
}
export default ButtonFormComponent

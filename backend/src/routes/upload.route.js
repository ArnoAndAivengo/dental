const express = require("express");
const router = express.Router();

const controller = require("../controller/fileUpload.controller");

let routes = (app) => {
  router.post("/uploads-file/:id", controller.uploadFile)
  router.post("/uploads-file/:id/avatar", controller.uploadFileAvatar)

  router.post("/delete-file/:id/:name", controller.deleteFile)
  router.post("/delete-file/:id/avatar/:name", controller.deleteFileAvatar)

  router.get("/files/:id", controller.getFilesList)
  router.get("/files/:id/avatar/", controller.getFileAvatar)

  router.get("/files/:id/:name", controller.downloadFiles)
  router.get("/files/:id/avatar/:name", controller.downloadFilesAvatar)

  app.use(router);
};

module.exports = routes;
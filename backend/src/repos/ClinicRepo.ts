import { EntityRepository, Repository } from "typeorm";
import { Clinic } from "../entity/Clinic";

@EntityRepository(Clinic)
export class ClinicRepo extends Repository<Clinic> {
  async findOrCreate({ id, ...data }: Partial<Clinic>) {
    let clinic = await this.findOne(id);

    if (!clinic) {
      clinic = await this.save({
        id,
        ...data
      });
    }

    return clinic;
  }
}

import { EntityRepository, Repository } from "typeorm";
import { Dentist } from "../entity/Dentist";

@EntityRepository(Dentist)
export class DentistRepo extends Repository<Dentist> {}

import { v4 } from "uuid";
import { redis } from "../../redis";
import { confirmDentistPrefix } from "../constants/redisPrefixes";

export const createConfirmationUrl = async (dentistId: number) => {
  const token = v4();
  await redis.set(confirmDentistPrefix + token, dentistId, "ex", 60 * 60 * 24); // 1 day expiration

  return `http://localhost:3000/dentist/confirm/${token}`;
};

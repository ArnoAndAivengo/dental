import nodemailer from "nodemailer";

export async function sendEmail(email: string, url: string) {
  // const account = await nodemailer.createTestAccount();

  const transporter = nodemailer.createTransport({
    host: "smtp.yandex.ru",
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
      user: 'vasterra-n@yandex.ru', // generated ethereal dentist
      pass: 'Ukt$9zxqR.9g/4)' // generated ethereal password
    }
  });

  const mailOptions = {
    from: '"Fred Foo 👻" <vasterra-n@yandex.ru>>', // sender address
    to: email, // list of receivers
    subject: "Test ✔", // Subject line
    text: "Test Test Test", // plain text body
    html: `<a href="${url}">${url}</a>` // html body
  };

  // const transporter = nodemailer.createTransport({
  //   host: "smtp.ethereal.email",
  //   port: 587,
  //   secure: false, // true for 465, false for other ports
  //   auth: {
  //     user: account.user, // generated ethereal user
  //     pass: account.pass // generated ethereal password
  //   }
  // });
  //
  // const mailOptions = {
  //   from: '"Fred Foo 👻" <foo@example.com>', // sender address
  //   to: email, // list of receivers
  //   subject: "Hello ✔", // Subject line
  //   text: "Hello world?", // plain text body
  //   html: `<a href="${url}">${url}</a>` // html body
  // };

  const info = await transporter.sendMail(mailOptions);
  console.log('info', info)
  console.log("Message sent: %s", info.messageId);
  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
}

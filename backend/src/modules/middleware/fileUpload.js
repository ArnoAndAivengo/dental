const util = require("util");
const multer = require("multer");
// var watermark = require('jimp-watermark');
let fs = require('fs-extra');
let DIR = './public/uploads/images/';

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    fs.mkdirsSync(DIR + req.params.id);
    cb(null, DIR + req.params.id);
  },
  filename: async (req, file, cb) => {
    const fileName = file.originalname.toLowerCase().split(' ').join('-');
    await cb(null, fileName)
    // var options = {
    //   'text': 'dental',
    //   'textSize': 6,
    //   'dstPath' : DIR + req.params.id + '/' + fileName
    // };
    // await watermark.addTextWatermark(DIR + req.params.id + '/' + fileName, options).then(data => {
    //   console.log(data);
    //   console.log(fileName);
    //
    // }).catch(err => {
    //   console.log(err);
    // });

  },
});

let upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      return cb(new Error('File types allowed .jpeg, .jpg and .png!'));
    }
  }
}).single("file");

let fileUploadMiddleware = util.promisify(upload);

module.exports = fileUploadMiddleware;

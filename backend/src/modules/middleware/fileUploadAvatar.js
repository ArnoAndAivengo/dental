const util = require("util");
const multer = require("multer");
let fs = require('fs-extra');
let DIR = './public/uploads/';
let DIRAvatar = './public/uploads/avatars/';

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    fs.removeSync(DIRAvatar + req.params.id);
    fs.mkdirsSync(DIRAvatar + req.params.id);
    cb(null, DIRAvatar + req.params.id);
  },
  filename: (req, file, cb) => {
    if (file !== undefined) {
      const fileName = file.originalname.toLowerCase().split(' ').join('-');
      cb(null, fileName)
    } else {
      fs.copySync(DIR, DIRAvatar + req.params.id)
    }

  },
});

let upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: (req, file, cb) => {
    if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
      cb(null, true);
    } else {
      cb(null, false);
      return cb(new Error('File types allowed .jpeg, .jpg and .png!'));
    }
  }
}).single("file");

let fileUploadMiddleware = util.promisify(upload);

module.exports = fileUploadMiddleware;

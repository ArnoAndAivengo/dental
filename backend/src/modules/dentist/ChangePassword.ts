import { Resolver, Mutation, Arg, Ctx } from "type-graphql";
import bcrypt from "bcryptjs";
import { Dentist } from "../../entity/Dentist";
import { redis } from "../../redis";
import { forgotPasswordPrefix } from "../constants/redisPrefixes";
import { ChangePasswordInput } from "./changePassword/ChangePasswordInput";
import { MyContext } from "../../types/MyContext";

@Resolver()
export class ChangePasswordResolver {
  @Mutation(() => Dentist, { nullable: true })
  async changePassword(
    @Arg("data")
    { token, password }: ChangePasswordInput,
    @Ctx() ctx: MyContext
  ): Promise<Dentist | null> {
    const dentistId = await redis.get(forgotPasswordPrefix + token);

    if (!dentistId) {
      return null;
    }

    const dentist = await Dentist.findOne(dentistId);

    if (!dentist) {
      return null;
    }

    await redis.del(forgotPasswordPrefix + token);

    dentist.password = await bcrypt.hash(password, 12);

    await dentist.save();

    ctx.req.session!.dentistId = dentist.id;

    return dentist;
  }
}

import { Resolver, Mutation, Arg } from "type-graphql";
import { redis } from "../../redis";
import { Dentist } from "../../entity/Dentist";
import { confirmDentistPrefix } from "../constants/redisPrefixes";

@Resolver()
export class ConfirmDentistResolver {
  @Mutation(() => Boolean)
  async confirmDentist(@Arg("token") token: string): Promise<boolean> {
    const dentistId = await redis.get(confirmDentistPrefix + token);

    if (!dentistId) {
      return false;
    }

    await Dentist.update({ id: parseInt(dentistId, 10) }, { confirmed: true });
    await redis.del(token);

    return true;
  }
}

import {Field, InputType} from "type-graphql";
import { PasswordMixin } from "../../shared/PasswordInput";
import {Column} from "typeorm";

@InputType()
export class addSettings extends PasswordMixin(class {}) {
  @Field({ nullable: true })
  @Column("text", { default: '' })
  firstName?: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  lastName?: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  phone?: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  qualifications?: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  bio?: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  website?: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  city?: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  street?: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  postIndex?: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  email?: string;
}

import bcrypt from "bcryptjs";
import { Arg, Ctx, Mutation, Resolver } from "type-graphql";
import { Dentist } from "../../entity/Dentist";
import { MyContext } from "../../types/MyContext";

@Resolver()
export class LoginResolver {
  @Mutation(() => Dentist, { nullable: true })
  async login(
    @Arg("email") email: string,
    @Arg("password") password: string,
    @Ctx() ctx: MyContext
  ): Promise<Dentist | null> {
    const dentist = await Dentist.findOne({ where: { email } });

    if (!dentist) {
      return null;
    }

    const valid = await bcrypt.compare(password, dentist.password);

    if (!valid) {
      return null;
    }

    if (!dentist.confirmed) {
      return null;
    }

    ctx.req.session!.dentistId = dentist.id;

    return dentist;
  }
}

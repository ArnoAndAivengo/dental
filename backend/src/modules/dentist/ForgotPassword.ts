import { Resolver, Mutation, Arg } from "type-graphql";
import { v4 } from "uuid";

import { sendEmail } from "../utils/sendEmail";
import { Dentist } from "../../entity/Dentist";
import { redis } from "../../redis";
import { forgotPasswordPrefix } from "../constants/redisPrefixes";

@Resolver()
export class ForgotPasswordResolver {
  @Mutation(() => Boolean)
  async forgotPassword(@Arg("email") email: string): Promise<boolean> {
    const dentist = await Dentist.findOne({ where: { email } });

    if (!dentist) {
      return true;
    }

    const token = v4();
    await redis.set(forgotPasswordPrefix + token, dentist.id, "ex", 60 * 60 * 24); // 1 day expiration

    await sendEmail(
      email,
      `http://localhost:3000/dentist/change-password/${token}`
    );

    return true;
  }
}

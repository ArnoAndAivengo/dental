import {Length} from "class-validator";
import {Field, ID, InputType} from "type-graphql";

@InputType()
export class DentistProfileInput {
  @Field(() => ID!)
  id: number;

  @Field({ nullable: true })
  @Length(1, 255)
  firstName?: string;

  @Field({ nullable: true })
  @Length(1, 255)
  lastName?: string;

  @Field({ nullable: true })
  phone?: string;

  @Field({ nullable: true })
  qualifications?: string;

  @Field({ nullable: true })
  bio?: string;

  @Field({ nullable: true })
  website?: string;

  @Field({ nullable: true })
  city?: string;

  @Field({ nullable: true })
  street?: string;

  @Field({ nullable: true })
  postIndex?: string;

  @Field({ nullable: true })
  email?: string;
}

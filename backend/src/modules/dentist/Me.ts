import { Resolver, Query, Ctx } from "type-graphql";

import { Dentist } from "../../entity/Dentist";
import { MyContext } from "../../types/MyContext";

@Resolver()
export class MeResolver {
  @Query(() => Dentist, { nullable: true, complexity: 5 })
  async me(@Ctx() ctx: MyContext): Promise<Dentist | undefined> {
    if (!ctx.req.session!.dentistId) {
      return undefined;
    }

    return Dentist.findOne(ctx.req.session!.dentistId);
  }
}

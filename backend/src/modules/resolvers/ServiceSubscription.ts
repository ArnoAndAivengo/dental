import { IResolvers } from "graphql-tools";

import { Dentist } from "../../entity/Dentist";
import { stripe } from "../../utils/stripe";

export const createSubscription: IResolvers = {
  Mutation: {
    createSubscription: async (_, { source, ccLast4 }, { req }) => {
      if (!req.session || !req.session.dentistId) {
        throw new Error("not authenticated");
      }

      const dentist = await Dentist.findOne(req.session.dentistId);

      if (!dentist) {
        throw new Error();
      }

      let stripeId = dentist.stripeId;

      if (!stripeId) {
        // @ts-ignore
        const customer = await stripe.customers.create({
          email: dentist.email,
          source,
          plan: process.env.PLAN
        });
        stripeId = customer.id;
      } else {
        // update customer
        await stripe.customers.update(stripeId, {
          source
        });
        await stripe.subscriptions.create({
          customer: stripeId,
          items: [
            {
              plan: process.env.PLAN!
            }
          ]
        });
      }

      dentist.stripeId = stripeId;
      dentist.type = "paid";
      dentist.ccLast4 = ccLast4;
      await dentist.save();

      return dentist;
    },
    changeCreditCard: async (_, { source, ccLast4 }, { req }) => {
      if (!req.session || !req.session.dentistId) {
        throw new Error("not authenticated");
      }

      const dentist = await Dentist.findOne(req.session.dentistId);

      if (!dentist || !dentist.stripeId || dentist.type !== "paid") {
        throw new Error();
      }

      await stripe.customers.update(dentist.stripeId, { source });

      dentist.ccLast4 = ccLast4;
      await dentist.save();

      return dentist;
    },
    cancelSubscription: async (_, __, { req }) => {
      if (!req.session || !req.session.dentistId) {
        throw new Error("not authenticated");
      }

      const dentist = await Dentist.findOne(req.session.dentistId);

      if (!dentist || !dentist.stripeId || dentist.type !== "paid") {
        throw new Error();
      }

      const stripeCustomer = await stripe.customers.retrieve(dentist.stripeId);

      // @ts-ignore
      const [subscription] = stripeCustomer.subscriptions.data;

      await stripe.subscriptions.del(subscription.id);


      // @ts-ignore
      await stripe.customers.deleteCard(
        dentist.stripeId,
        // @ts-ignore
        stripeCustomer.default_source as string
      );

      dentist.type = "free-trial";
      await dentist.save();

      return dentist;
    }
  }
};

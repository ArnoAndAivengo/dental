import {Arg, Int, Mutation, Query, Resolver} from "type-graphql";
import { Dentist } from "../../entity/Dentist";
import axios from "axios";
import {DentistProfileInput} from "../dentist/profile/DentistProfileInput";

@Resolver()
export class DentistResolver {

  @Mutation(() => Dentist)
  async createDentist(@Arg("name") name: string) {
    return Dentist.create({ name }).save();
  }

  @Mutation(() => Dentist)
  async addDentistSettings(
    @Arg("data")
      {
        id,
        firstName,
        lastName,
        phone,
        qualifications,
        bio,
        website,
        city,
        street,
        postIndex,
        email
      }: DentistProfileInput): Promise<Dentist> {
    const dentistData: any = await Dentist.findOne(parseInt(String(id)));

    try {
      if (dentistData) {
        dentistData.firstName = firstName;
        dentistData.lastName = lastName;
        dentistData.phone = phone;
        dentistData.qualifications = qualifications;
        dentistData.bio = bio;
        dentistData.website = website;
        dentistData.city = city;
        dentistData.street = street;
        dentistData.postIndex = postIndex;
        dentistData.email = email;

        if (city || street || postIndex) {
          await axios.get(encodeURI('https://maps.google.com/maps/api/geocode/json?sensor=false&address=' + city + ' ' + street + ' ' + postIndex +'&key=AIzaSyDMYrZZhMGlK5PKOMQRQMVffXnUJwgyatY'))
            .then(result => {
              dentistData.lng = result.data.results[0].geometry.location.lng;
              dentistData.lat = result.data.results[0].geometry.location.lat;
            })
            .catch((_error: any) => {
            })
        }

        await dentistData.save();

        return dentistData;
      }
    } catch (e) {
      throw e
    }

    return dentistData
  }

  @Query(() => [Dentist])
  async dentists() {
    return Dentist.find();
  }

  @Query(() => Dentist, { nullable: true })
  dentist(@Arg("id", () => Int) id: number): Promise<Dentist | undefined> {
    return Dentist.findOne(id);
  }
}

import {Arg, Int, Mutation, Query, Resolver} from "type-graphql";
import {DentistClinic} from "../../entity/DentistClinic";
import {Clinic} from "../../entity/Clinic";

@Resolver()
export class ClinicResolver {
  @Mutation(() => Clinic)
  async createClinic(@Arg("name") name: string) {
    return Clinic.create({ name }).save();
  }

  @Mutation(() => Boolean)
  async addDentistClinic(
    @Arg("dentistId", () => Int) dentistId: number,
    @Arg("clinicId", () => Int) clinicId: number
  ) {
    await DentistClinic.create({ dentistId, clinicId }).save();
    return true;
  }

  @Mutation(() => Boolean)
  async deleteClinic(@Arg("clinicId", () => Int) clinicId: number) {
    await Clinic.delete({ id: clinicId });
    return true;
  }

  @Query(() => [Clinic])
  async clinics() {
    return Clinic.find();
  }

  @Query(() => [Clinic])
  async service(
    @Arg("name") name: string,
  ){
    return Clinic.findOne(name);
  }

}

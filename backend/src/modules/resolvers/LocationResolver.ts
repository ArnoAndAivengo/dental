import {Arg, Float, Int, Mutation, Query, Resolver} from "type-graphql";
import {Coordinate} from "../../entity/Coordinate";
import {Location} from "../../entity/Location";

@Resolver()
export class LocationResolver {

  @Mutation(() => Coordinate)
  async createCoordinates(
    @Arg("lat", () => Float) lat: number,
    @Arg("lng", () => Float) lng: number
  ) {
    return Coordinate.create({ lat, lng }).save();
  }

  @Mutation(() => Boolean)
  async addDentistCoordinates(
    @Arg("dentistId", () => Int) dentistId: number,
    @Arg("coordinatesId", () => Int) coordinatesId: number
  ) {
    await Location.create({ dentistId, coordinatesId }).save();
    return true;
  }

  @Query(() => [Coordinate])
  async coordinates() {
    return Coordinate.find();
  }
}

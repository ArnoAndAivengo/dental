import {Arg, Int, Mutation, Query, Resolver} from "type-graphql";
import {Practice} from "../../entity/Practice";
import {PracticeToUser} from "../../entity/PracticeToUser";

@Resolver()
export class PracticeResolver {

  @Mutation(() => Boolean)
  async createPractice(
    @Arg("practice", () => String) name: string,
    @Arg("dentistId", () => Int) dentistId: number,
  ) {
    await Practice.create({ name }).save().then(e => {
      const practiceId = e.id
      PracticeToUser.create({ dentistId, practiceId }).save();
    });
    return true
  }

  @Mutation(() => Boolean)
  async addDentistPractice(
    @Arg("dentistId", () => Int) dentistId: number,
    @Arg("practiceId", () => Int) practiceId: number
  ) {
    await PracticeToUser.create({ dentistId, practiceId }).save();
    return true;
  }

  @Mutation(() => Boolean)
  async deletePractice(@Arg("practiceId", () => Int) practiceId: number) {
    await Practice.delete({ id: practiceId });
    return true;
  }

  @Query(() => [Practice])
  async practices() {
    return Practice.find();
  }

  @Query(() => [Practice])
  async practice(
    @Arg("practice") practice: string,
    ){
    return Practice.findOne(practice);
  }
}

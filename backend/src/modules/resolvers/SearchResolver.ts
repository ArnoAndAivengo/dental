import {Arg, Float, Query, Resolver} from "type-graphql";
import {Dentist} from "../../entity/Dentist";

@Resolver()
export class SearchResolver {
  @Query(() => [Dentist])
  async querySearchDentist(
    @Arg("lat", () => Float) lat: number,
    @Arg("lng", () => Float) lng: number,
  ){
    const dentists = await Dentist.find();
    return findCoordinatesDentists({lat, lng}, dentists)
  }
}

const findCoordinatesDentists = (coordinate: object, dentists: Dentist[]): object | [] => {
  let distanceDent: any[] = [];

  if (!dentists) {
    // @ts-ignore
    return
  }

  dentists.map((dent: { lng: any; lat: any; }) => {
    // @ts-ignore
    const a = {'Longitude': coordinate?.lng, 'Latitude': coordinate?.lat};
    const b = {'Longitude': dent.lng, 'Latitude': dent.lat};
    const distanceCur = (111.111 *
      (180 / Math.PI) * (
        Math.acos(Math.cos(a.Latitude * (Math.PI / 180))
          * Math.cos(b.Latitude * (Math.PI / 180))
          * Math.cos((a.Longitude - b.Longitude) * (Math.PI / 180))
          + Math.sin(a.Latitude * (Math.PI / 180))
          * Math.sin(b.Latitude * (Math.PI / 180)))))
    if (distanceCur < 100) {
      distanceDent.push(dent)
    }
  })
  return distanceDent
}

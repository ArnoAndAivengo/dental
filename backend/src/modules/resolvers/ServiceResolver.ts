import {Arg, Int, Mutation, Query, Resolver} from "type-graphql";
import {Service} from "../../entity/Service";
import {ServiceToUser} from "../../entity/ServiceToUser";

@Resolver()
export class ServiceResolver {

  @Mutation(() => Boolean)
  async createService(
    @Arg("service", () => String) name: string,
    @Arg("dentistId", () => Int) dentistId: number,
  ) {
    await Service.create({name}).save().then(e => {
      const serviceId = e.id
      ServiceToUser.create({dentistId, serviceId}).save();
    });
    return true
  }

  @Mutation(() => Boolean)
  async addDentistService(
    @Arg("dentistId", () => Int) dentistId: number,
    @Arg("serviceId", () => Int) serviceId: number
  ) {
    await ServiceToUser.create({dentistId, serviceId}).save();
    return true;
  }

  @Mutation(() => Boolean)
  async deleteService(@Arg("serviceId", () => Int) serviceId: number) {
    await Service.delete({id: serviceId});
    return true;
  }

  @Query(() => [Service])
  async services() {
    return Service.find();
  }

  @Query(() => [Service])
  async service(
    @Arg("service") service: string,
  ) {
    return Service.findOne(service);
  }
}

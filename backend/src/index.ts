import { ApolloServer } from "apollo-server-express";
import connectRedis from "connect-redis";
import cors from "cors";
import Express from "express";
import session from "express-session";
import "reflect-metadata";
import { Container } from "typedi";
import * as typeorm from "typeorm";
import { createConnection } from "typeorm";
import { redis } from "./redis";
import { createDentistLoader } from "./utils/dentistLoader";
import { createSchema } from "./utils/createSchema";

typeorm.useContainer(Container);

const main = async () => {
  await createConnection();

  const schema = await createSchema();

  const apolloServer = new ApolloServer({
    schema,
    context: ({ req, res }: any) => ({
      req,
      res,
      dentistsLoader: createDentistLoader()
    }),
    tracing: true

  });

  const app = Express();

  app.use(
    cors({
      credentials: true,
      origin: "http://localhost:3000"
    })
  );

  const RedisStore = connectRedis(session);

  const evokeRoutes = require("./routes/upload.route");
  app.use(Express.urlencoded({
    extended: true
  }));

  evokeRoutes(app);

  app.use(
    session({
      store: new RedisStore({
        client: redis as any
      }),
      name: "qid",
      secret: "aslkdfjoiq12312",
      resave: false,
      saveUninitialized: false,
      cookie: {
        httpOnly: true,
        secure: process.env.NODE_ENV === "production",
        maxAge: 1000 * 60 * 60 * 24 * 7 * 365 // 7 years
      }
    })
  );

  apolloServer.applyMiddleware({ app, cors: false });

  app.listen(4000, () => {
    console.log("server started on http://localhost:4000/graphql");
  });
};

main().catch(err => console.error(err));



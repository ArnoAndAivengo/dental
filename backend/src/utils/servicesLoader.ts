import DataLoader from "dataloader";
import { In } from "typeorm";
import { Dentist } from "../entity/Dentist";
import {DentistService} from "../entity/DentistService";

const batchDentists = async (serviceIds: number[]) => {
  const dentistServices = await DentistService.find({
    join: {
      alias: "dentistService",
      innerJoinAndSelect: {
        dentist: "dentistService.dentist"
      }
    },
    where: {
      serviceId: In(serviceIds)
    },
    cache: true
  });

  const serviceIdToDentist: { [key: number]: Dentist[] } = {};

  dentistServices.forEach(ab => {
    if (ab.serviceId in serviceIdToDentist) {
      serviceIdToDentist[ab.serviceId].push((ab as any).__dentist__);
    } else {
      serviceIdToDentist[ab.serviceId] = [(ab as any).__dentist__];
    }
  });

  return serviceIds.map(serviceId => serviceIdToDentist[serviceId] || []);
};

export const createServiceLoader = () => new DataLoader(batchDentists);

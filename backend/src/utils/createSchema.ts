import { buildSchema } from "type-graphql";
import { ClinicResolver } from "../modules/resolvers/ClinicResolver";
import { DentistResolver } from "../modules/resolvers/DentistResolver";
import { ServiceResolver } from "../modules/resolvers/ServiceResolver";
import { LocationResolver } from "../modules/resolvers/LocationResolver";
import { ChangePasswordResolver } from "../modules/dentist/ChangePassword";
import { ConfirmDentistResolver } from "../modules/dentist/ConfirmDentist";
import { CreateDentistResolver } from "../modules/dentist/CreateDentist";
import { SearchResolver } from "../modules/resolvers/SearchResolver";
import { PracticeResolver } from "../modules/resolvers/PracticeResolver";
// import {createSubscription} from "../modules/resolvers/ServiceSubscription";
import { ForgotPasswordResolver } from "../modules/dentist/ForgotPassword";
import { LoginResolver } from "../modules/dentist/Login";
import { LogoutResolver } from "../modules/dentist/Logout";
import { MeResolver } from "../modules/dentist/Me";
import { ProfilePictureResolver } from "../modules/dentist/ProfilePicture";
import { RegisterResolver } from "../modules/dentist/Register";
import { Container } from "typedi";

export const createSchema = () =>
  buildSchema({
    container: Container,
    resolvers: [
      ChangePasswordResolver,
      ConfirmDentistResolver,
      ForgotPasswordResolver,
      LoginResolver,
      LogoutResolver,
      MeResolver,
      RegisterResolver,
      CreateDentistResolver,
      SearchResolver,
      PracticeResolver,
      // createSubscription,
      ServiceResolver,
      ProfilePictureResolver,
      ClinicResolver,
      DentistResolver,
      LocationResolver
    ],
    authChecker: ({ context: { req } }) => {
      return !!req.session.dentistId;
    }
  });
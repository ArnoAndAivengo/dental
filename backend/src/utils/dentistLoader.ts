import DataLoader from "dataloader";
import { In } from "typeorm";
import { Dentist } from "../entity/Dentist";
import { DentistClinic } from "../entity/DentistClinic";

const batchDentists = async (clinicIds: number[]) => {
  const dentistClinics = await DentistClinic.find({
    join: {
      alias: "dentistClinic",
      innerJoinAndSelect: {
        dentist: "dentistClinic.dentist"
      }
    },
    where: {
      clinicId: In(clinicIds)
    },
    cache: true
  });

  const clinicIdToDentists: { [key: number]: Dentist[] } = {};

  dentistClinics.forEach(ab => {
    if (ab.clinicId in clinicIdToDentists) {
      clinicIdToDentists[ab.clinicId].push((ab as any).__dentist__);
    } else {
      clinicIdToDentists[ab.clinicId] = [(ab as any).__dentist__];
    }
  });

  return clinicIds.map(clinicId => clinicIdToDentists[clinicId] || []);
};

export const createDentistLoader = () => new DataLoader(batchDentists);

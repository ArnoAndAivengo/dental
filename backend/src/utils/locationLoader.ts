import DataLoader from "dataloader";
import { In } from "typeorm";
import { Dentist } from "../entity/Dentist";
import { DentistCoordinate } from "../entity/DentistCoordinate";

const batchDentists = async (coordinateIds: number[]) => {
  const dentistClinics = await DentistCoordinate.find({
    join: {
      alias: "dentistCoordinate",
      innerJoinAndSelect: {
        dentist: "dentistCoordinate.dentist"
      }
    },
    where: {
      coordinateId: In(coordinateIds)
    },
    cache: true
  });

  const coordinateIdToDentist: { [key: number]: Dentist[] } = {};

  dentistClinics.forEach(ab => {
    if (ab.coordinateId in coordinateIdToDentist) {
      coordinateIdToDentist[ab.coordinateId].push((ab as any).__dentist__);
    } else {
      coordinateIdToDentist[ab.coordinateId] = [(ab as any).__dentist__];
    }
  });

  return coordinateIds.map(coordinateId => coordinateIdToDentist[coordinateId] || []);
};

export const createLocationLoader = () => new DataLoader(batchDentists);

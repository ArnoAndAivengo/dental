import {Field, ID, ObjectType} from "type-graphql";
import {BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {ServiceToUser} from "./ServiceToUser";

@ObjectType()
@Entity()
export class Service extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column("text", {default: ''})
  name: string;

  @Field(() => [ServiceToUser])
  @OneToMany(() => ServiceToUser, ab => ab.service)
  dentists: Promise<ServiceToUser[]>;
}

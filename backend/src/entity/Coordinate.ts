import {Field, Float, ID, ObjectType} from "type-graphql";
import {BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Location} from "./Location";

@ObjectType()
@Entity()
export class Coordinate extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field({nullable: true})
  @Column("text", {default: 'Point'})
  type: string;

  @Field(() => Float)
  @Column({type: "float"})
  lat: number;

  @Field(() => Float)
  @Column({type: "float"})
  lng: number;

  @Field(() => [Location])
  @OneToMany(() => Location, ab => ab.coordinates)
  dentistConnection: Promise<Location[]>;

}


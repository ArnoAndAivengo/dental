import {
  BaseEntity,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn
} from "typeorm";
import { Dentist } from "./Dentist";
import {Coordinate} from "./Coordinate";

@Entity()
export class DentistCoordinate extends BaseEntity {
  @PrimaryColumn()
  dentistId: number;

  @PrimaryColumn()
  coordinateId: number;

  @ManyToOne(() => Dentist, coordinate => coordinate.coordinateConnection, { primary: true })
  @JoinColumn({ name: "dentistId" })
  dentist: Promise<Dentist>;

  @ManyToOne(() => Coordinate, dentist => dentist.dentistConnection, {
    primary: true
  })
  @JoinColumn({ name: "coordinateId" })
  coordinate: Promise<Coordinate>;
}

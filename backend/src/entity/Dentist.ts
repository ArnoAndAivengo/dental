import {BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Field, Float, ID, ObjectType, Root} from "type-graphql";
import {DentistClinic} from "./DentistClinic";
// import { Service } from "./Service";
import {Location} from "./Location";
import {ServiceToUser} from "./ServiceToUser";
import {PracticeToUser} from "./PracticeToUser";

@ObjectType()
@Entity()
export class Dentist extends BaseEntity {

  @Field(() => ID)
  @PrimaryGeneratedColumn()
  readonly id: number;

  @Column("text", { nullable: true })
  stripeId: string | null;

  @Column("text", { default: "free-trial" })
  type: string;

  @Column("text", { nullable: true })
  ccLast4: string | null;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  firstName?: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  lastName?: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  phone?: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  qualifications?: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  bio?: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  website?: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  city?: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  street?: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  postIndex?: string;

  @Field()
  @Column("text", { unique: true })
  email: string;

  @Column()
  password: string;

  @Field({ nullable: true })
  @Column("text", { default: '' })
  practiceName?: string;

  @Field(() => Float)
  @Column( "float", { default: 0.0 })
  lat: number;

  @Field(() => Float)
  @Column("float", { default: 0.0 })
  lng: number;

  @Field()
  registrationDate: Date;

  @Field({ complexity: 3 })
  name(@Root() parent: Dentist): string {
    return `${parent.firstName} ${parent.lastName}`;
  }

  @Field({ complexity: 3 })
  address(@Root() parent: Dentist): string {
    return `${parent.city} ${parent.street} ${parent.postIndex}`;
  }

  @Column("bool", { default: false })
  confirmed: boolean;

  @OneToMany(() => Location, ab => ab.dentist)
  coordinateConnection: Promise<Location[]>

  @OneToMany(() => DentistClinic, ab => ab.dentist)
  clinicConnection: Promise<DentistClinic[]>

  @Field(() => [ServiceToUser])
  @OneToMany(() => ServiceToUser, ab => ab.dentist)
  services: Promise<ServiceToUser[]>;

  @Field(() => [PracticeToUser])
  @OneToMany(() => PracticeToUser, ab => ab.dentist)
  practices: Promise<PracticeToUser[]>;

}

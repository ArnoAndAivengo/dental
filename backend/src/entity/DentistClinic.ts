import {BaseEntity, Entity, JoinColumn, ManyToOne, PrimaryColumn} from "typeorm";
import {Dentist} from "./Dentist";
import {Clinic} from "./Clinic";

@Entity()
export class DentistClinic extends BaseEntity {
  @PrimaryColumn()
  dentistId: number;

  @PrimaryColumn()
  clinicId: number;

  @ManyToOne(() => Dentist, clinic => clinic.clinicConnection, {primary: true})
  @JoinColumn({name: "dentistId"})
  dentist: Promise<Dentist>;

  @ManyToOne(() => Clinic, dentist => dentist.dentistConnection, {
    primary: true
  })
  @JoinColumn({name: "clinicId"})
  clinic: Promise<Clinic>;
}

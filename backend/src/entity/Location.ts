import {Field, ObjectType} from "type-graphql";
import {BaseEntity, Entity, JoinColumn, ManyToOne, PrimaryColumn} from "typeorm";
import {Coordinate} from "./Coordinate";
import {Dentist} from "./Dentist";

@ObjectType()
@Entity()
export class Location extends BaseEntity {
  @PrimaryColumn()
  dentistId: number;

  @PrimaryColumn()
  coordinatesId: number;

  @Field(() => Dentist)
  @ManyToOne(() => Dentist, dentist => dentist.coordinateConnection, {
    onDelete: "CASCADE"
  })
  @JoinColumn({name: "dentistId"})
  dentist: Promise<Dentist>;

  @ManyToOne(() => Coordinate, coordinates => coordinates.dentistConnection, {
    onDelete: "CASCADE"
  })
  @JoinColumn({name: "coordinatesId"})
  coordinates: Promise<Coordinate>;
}


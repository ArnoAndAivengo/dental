import { Ctx, Field, ID, ObjectType } from "type-graphql";
import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn
} from "typeorm";
import { MyContext } from "../types/MyContext";
import { Dentist } from "./Dentist";
import { DentistClinic } from "./DentistClinic";

@ObjectType()
@Entity()
export class Clinic extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column("text", { default: '' , unique: true })
  name: string;

  @OneToMany(() => DentistClinic, ab => ab.clinic)
  dentistConnection: Promise<DentistClinic[]>;

  @Field(() => [Dentist])
  async dentists(@Ctx() { dentistLoader }: MyContext): Promise<Dentist[]> {
    return dentistLoader.load(this.id);
  }
}

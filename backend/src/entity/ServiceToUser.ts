import {Field, ObjectType} from "type-graphql";
import {BaseEntity, Entity, JoinColumn, ManyToOne, PrimaryColumn} from "typeorm";
import {Dentist} from "./Dentist";
import {Service} from "./Service";

@ObjectType()
@Entity()
export class ServiceToUser extends BaseEntity {
  @PrimaryColumn()
  dentistId: number;

  @PrimaryColumn()
  serviceId: number;

  @Field(() => Dentist)
  @ManyToOne(() => Dentist, dentist => dentist.services, {
    onDelete: "CASCADE"
  })
  @JoinColumn({name: "dentistId"})
  dentist: Promise<Dentist>;

  @Field(() => Service)
  @ManyToOne(() => Service, service => service.dentists, {
    onDelete: "CASCADE"
  })
  @JoinColumn({name: "serviceId"})
  service: Promise<Service>;
}


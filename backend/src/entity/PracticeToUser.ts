import {Field, ObjectType} from "type-graphql";
import {BaseEntity, Entity, JoinColumn, ManyToOne, PrimaryColumn} from "typeorm";
import {Dentist} from "./Dentist";
import {Practice} from "./Practice";

@ObjectType()
@Entity()
export class PracticeToUser extends BaseEntity {
  @PrimaryColumn()
  dentistId: number;

  @PrimaryColumn()
  practiceId: number;

  @Field(() => Dentist)
  @ManyToOne(() => Dentist, dentist => dentist.practices, {
    onDelete: "CASCADE"
  })
  @JoinColumn({name: "dentistId"})
  dentist: Promise<Dentist>;

  @Field(() => Practice)
  @ManyToOne(() => Practice, practice => practice.dentists, {
    onDelete: "CASCADE"
  })
  @JoinColumn({name: "practiceId"})
  practice: Promise<Practice>;
}


import {Field, ID, ObjectType} from "type-graphql";
import {BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {PracticeToUser} from "./PracticeToUser";

@ObjectType()
@Entity()
export class Practice extends BaseEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column("text", {default: ''})
  name: string;

  @Field(() => [PracticeToUser])
  @OneToMany(() => PracticeToUser, ab => ab.practice)
  dentists: Promise<PracticeToUser[]>;
}

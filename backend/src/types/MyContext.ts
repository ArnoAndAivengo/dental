import {Request, Response} from "express";
import {createDentistLoader} from "../utils/dentistLoader";
import {createLocationLoader} from "../utils/locationLoader";
import {createServiceLoader} from "../utils/servicesLoader";

export interface MyContext {
  req: Request;
  res: Response;
  dentistLoader: ReturnType<typeof createDentistLoader>;
  locationLoader: ReturnType<typeof createLocationLoader>;
  serviceLoader: ReturnType<typeof createServiceLoader>;
}

declare module "express-session" {
  interface Session {
    dentistId: number;
  }
}
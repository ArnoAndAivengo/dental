const upload = require("../modules/middleware/fileUpload");
const uploadAvatar = require("../modules/middleware/fileUploadAvatar");
const URL = "http://localhost:4000/files/";
const fs = require("fs");

const uploadFile = async (req, res) => {
  // res.header('Access-Control-Allow-Origin: *');
  // res.header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
  // res.header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

  try {
    await upload(req, res);

    if (req.file == undefined) {
      return res.status(400).send({message: "Choose a file to uploads"});
    }

    res.status(200).send({
      message: "File uploaded successfully: " + req.file.originalname,
    });
  } catch (err) {
    console.log(err);

    if (err.code == "LIMIT_FILE_SIZE") {
      return res.status(500).send({
        message: "File size should be less than 5MB",
      });
    }

    res.status(500).send({
      message: `Error occured: ${err}`,
    });
  }
};

const uploadFileAvatar = async (req, res) => {
  // res.header('Access-Control-Allow-Origin: *');
  // res.header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
  // res.header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

  try {
    await uploadAvatar(req, res);

    if (req.file == undefined) {
      return res.status(400).send({message: "Choose a file to uploads"});
    }

    res.status(200).send({
      message: "File uploaded successfully: " + req.file.originalname,
    });
  } catch (err) {
    console.log(err);

    if (err.code == "LIMIT_FILE_SIZE") {
      return res.status(500).send({
        message: "File size should be less than 5MB",
      });
    }

    res.status(500).send({
      message: `Error occured: ${err}`,
    });
  }
};

const deleteFile = async (req, res) => {
  try {
    fs.unlinkSync("./public/uploads/images/" + req.params.id + '/' + req.params.name)

    res.status(200).send({
      message: "File remove successfully: " + req.params.name,
    });
  } catch (err) {
    console.log(err);

    res.status(500).send({
      message: `Error occurred: ${err}`,
    });
  }
};

const deleteFileAvatar = async (req, res) => {
  try {
    fs.unlinkSync("./public/uploads/images/" + req.params.id + '/avatar/' + req.params.name)

    res.status(200).send({
      message: "File remove successfully: " + req.params.name,
    });
  } catch (err) {
    console.log(err);

    res.status(500).send({
      message: `Error occurred: ${err}`,
    });
  }
};

const getFilesList = (req, res) => {
  const path = "./public/uploads/images/" + req.params.id;

  fs.readdir(path, function (err, files) {
    if (err) {
      res.status(500).send({
        message: "Files not found.",
        status: 500
      });
    }

    let filesList = [];

    if (files !== undefined) {
      files.forEach((file, key) => {
        filesList.push({
          thumbnail: URL + req.params.id + '/' + file,
          src: URL + req.params.id + '/' + file,
          name: file,
          thumbnailWidth: 320,
          thumbnailHeight: 212,
          isSelected: false
        });
      });
    } else {
      return
    }

    res.status(200).send(filesList);
  });
};

const getFileAvatar = (req, res) => {
  const path = "./public/uploads/avatars/" + req.params.id + '/';
  console.log(path)
  fs.readdir(path, function (err, files) {
    if (err) {
      res.status(500).send({
        message: "Files not found.",
        status: 500
      });
    }

    let filesList = [];

    if (files !== undefined) {
      filesList.push({
        src: URL + req.params.id + '/avatar/' + files[0],
      });
    } else {
      return
    }

    res.status(200).send(filesList);
  });
};

const downloadFiles = (req, res) => {
  const fileName = req.params.name;
  const path = "./public/uploads/images/" + req.params.id + "/";

  res.download(path + fileName, (err) => {
    if (err) {
      res.status(500).send({
        message: "File can not be downloaded: " + err,
      });
    }
  });
};

const downloadFilesAvatar = (req, res) => {
  const fileName = req.params.name;
  const path = "./public/uploads/avatars/" + req.params.id + '/';

  res.download(path + fileName, (err) => {
    if (err) {
      res.status(500).send({
        message: "File can not be downloaded: " + err,
      });
    }
  });
};

module.exports = {uploadFile, uploadFileAvatar, deleteFile, deleteFileAvatar, downloadFiles, downloadFilesAvatar, getFilesList, getFileAvatar};
